<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\bdc\home;
use app\bdc\model\Qun as QunModel;
use app\bdc\model\Setting as SettingModel;


/**
 * 前台首页控制器
 * @package app\bdc\index
 */
class Index extends Common
{
    /**
     * 首页
     * @author 蔡伟明 <314013107@qq.com>
     * @return mixed
     */
    public function index()
    {
        $level = input('param.level/d', 1);

        $setting = SettingModel::where(['id'=>1])->find();
        $qun_list = QunModel::where(['status'=>1, 'level'=>$level])->select();
        $level_arr = get_level();
        $level_list = [];
        foreach($level_arr as $key=>$val){
            $level_list[$key]['level'] = $key;
            $level_list[$key]['name'] = $val;
            $level_list[$key]['num'] = QunModel::where(['status'=>1, 'level'=>$key])->count();
        }

        //总量
        $total_chicang = QunModel::where(['status'=>1])->sum('chicang');
        $total_fensi = QunModel::where(['status'=>1])->sum('fensi');
        $total_qun = QunModel::where(['status'=>1])->count();

        $this->assign('total_chicang', $total_chicang);
        $this->assign('total_fensi', $total_fensi);
        $this->assign('total_qun', $total_qun);

        $this->assign('level',$level);
        $this->assign('setting',$setting);
        $this->assign('qun_list',$qun_list);
        $this->assign('level_list',$level_list);

        return $this->fetch(); // 渲染模板
    }

    /**
     * 公告详情
     * @author 蔡伟明 <314013107@qq.com>
     * @return mixed
     */
    public function announce()
    {
        $setting = SettingModel::where(['id'=>1])->find();
        $setting['content'] = html_entity_decode($setting['content']);
        $this->assign('setting', $setting);
        return $this->fetch(); // 渲染模板
    }

    /**
     * 生成证书
     * @author 蔡伟明 <314013107@qq.com>
     * @return mixed
     */
    public function certificate()
    {
        $id = input('param.id/d');
        if(!$id){
            $this->error('该群不存在，请稍后再试');
        }
        $data = QunModel::where(['id'=>$id])->find();

        //生成证书
        $cert = $this->createCert($data);
        $cert_file = 'http://bdc.rockyshi.com/'.$cert;
        $this->assign('cert_file', $cert_file);

        return $this->fetch(); // 渲染模板
    }

    /**
     * 证书生成
     * @param $data
     * @param $fileName string 保存文件名,默认空则直接输入图片
     */
    function createCert($data, $fileName = '')
    {
        ob_clean();

        $image = imagecreatefrompng(ROOT_PATH.'application/bdc/public/static/bdc/img/'.$data['level'].'.png'); // 证书模版图片文件的路径
        $color = imagecolorallocate($image,28,28,28);   // 字体颜色

        $lanting = ROOT_PATH.'application/bdc/public/static/bdc/ttf/lantingeb.ttf';
        $taile = ROOT_PATH.'application/bdc/public/static/bdc/ttf/taile.ttf';

        // imageTTFText("Image", "Font Size", "Rotate Text", "Left Position","Top Position", "Font Color", "Font Name", "Text To Print");

        //编号
        imagettftext($image, 14, 0, 257, 478, $color, $lanting, $data['bianhao']);
        imagettftext($image, 14, 0, 332, 564, $color, $taile, $data['bianhao']);

        //姓名
        imagettftext($image, 14, 0, 210, 453, $color, $lanting, $data['weixin']);
        imagettftext($image, 14, 0, 305, 512, $color, $taile, $data['weixinen']);

        //时间
        imagettftext($image, 11, 0, 395, 671, $color, $taile, $data['cert_time']);

        // if you want to save the file in the web server
        $uploads_file = ROOT_PATH . 'public' . DS. 'uploads/cert/';
        $filename = 'certificate_'.$data['id'].'.png';
        imagepng($image, $uploads_file.$filename);
        imagedestroy($image);

        return './uploads/cert/'.$filename;

        /* If you want to display the file in browser */
        /*
        header('Content-type: image/png;');
        imagepng($image);
        imagedestroy($image);
        exit;
        */

        // If you want the user to download the file
        /*
        imagepng($image,$filename);
        header('Pragma: public');
        header('Cache-Control: public, no-cache');
        header('Content-Type: application/octet-stream');
        header('Content-Length: ' . filesize($filename));
        header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
        header('Content-Transfer-Encoding: binary');
        readfile($filename);
        imagedestroy($image);
        */
    }
}