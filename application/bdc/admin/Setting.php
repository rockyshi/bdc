<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\bdc\admin;

use app\admin\controller\Admin;
use app\common\builder\ZBuilder;
use app\bdc\model\Setting as SettingModel;

/**
 * 基础设置控制器
 * @package app\bdc\admin
 */
class Setting extends Admin
{

    /**
     * 编辑
     * @param 1 $id 设置id
     * @author 蔡伟明 <314013107@qq.com>
     * @return mixed
     */
    public function index($id = 1)
    {
        if ($id === null) $this->error('缺少参数');

        // 保存数据
        if ($this->request->isPost()) {
            // 表单数据
            $data = $this->request->post();

            // 验证
            $result = $this->validate($data, 'Setting');
            if(true !== $result) $this->error($result);

            if (SettingModel::update($data)) {
                // 记录行为
                action_log('setting_add', 'bdc_setting', $id, UID, $data['title']);
                $this->success('编辑成功', 'index');
            } else {
                $this->error('编辑失败');
            }
        }

        $level_list = get_level();
        $info = SettingModel::get($id);

        // 显示编辑页面
        return ZBuilder::make('form')
            ->addFormItems([
                ['hidden', 'id', 1],
                ['text', 'wangzhi', '交易所网址'],
                ['text', 'lianxi', '联系方式'],
                ['image', 'qrcode', '二维码'],
                ['text', 'title', '公告标题'],
                ['ueditor', 'content', '公告内容'],
            ])
            ->setFormData($info)
            ->fetch();
    }
}