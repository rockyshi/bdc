<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

namespace app\bdc\validate;

use think\Validate;

/**
 * 滚动图片验证器
 * @package app\bdc\validate
 * @author 蔡伟明 <314013107@qq.com>
 */
class Setting extends Validate
{
    // 定义验证规则
    protected $rule = [
        'title|公告标题' => 'require',
        'content|公告内容' => 'require',
        'wangzhi|交易所网址' => 'require',
        'lianxi|联系方式' => 'require',
    ];

    // 定义验证场景
    protected $scene = [
        'title' => ['title'],
    ];
}
