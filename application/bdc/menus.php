<?php
// +----------------------------------------------------------------------
// | 海豚PHP框架 [ DolphinPHP ]
// +----------------------------------------------------------------------
// | 版权所有 2016~2017 河源市卓锐科技有限公司 [ http://www.zrthink.com ]
// +----------------------------------------------------------------------
// | 官方网站: http://dolphinphp.com
// +----------------------------------------------------------------------
// | 开源协议 ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------

/**
 * 菜单信息
 */
return [
    [
        'title'       => '社群',
        'icon'        => 'fa fa-fw fa-shopping-cart',
        'url_type'    => 'module_admin',
        'url_value'   => 'bdc/index/index',
        'url_target'  => '_self',
        'online_hide' => 0,
        'sort'        => 100,
        'child'       => [
		
            [
                'title'       => '系统管理',
                'icon'        => 'fa fa-fw fa-users',
                'url_type'    => 'module_admin',
                'url_value'   => '',
                'url_target'  => '_self',
                'online_hide' => 0,
                'sort'        => 100,
                'child'       => [
                    [
                        'title'       => '基础设置',
                        'icon'        => 'fa fa-fw fa-list',
                        'url_type'    => 'module_admin',
                        'url_value'   => 'bdc/setting/index',
                        'url_target'  => '_self',
                        'online_hide' => 0,
                        'sort'        => 100
                    ],
                ],
            ],
		
            [
                'title'       => '社群管理',
                'icon'        => 'fa fa-fw fa-shopping-cart',
                'url_type'    => 'module_admin',
                'url_value'   => 'bdc/index/index',
                'url_target'  => '_self',
                'online_hide' => 0,
                'sort'        => 100,
                'child'       => [
                    [
                        'title'       => '社群列表',
                        'icon'        => 'fa fa-fw fa-list',
                        'url_type'    => 'module_admin',
                        'url_value'   => 'bdc/qun/index',
                        'url_target'  => '_self',
                        'online_hide' => 0,
                        'sort'        => 100,
                        'child'       => [
                            [
                                'title'       => '编辑',
                                'icon'        => '',
                                'url_type'    => 'module_admin',
                                'url_value'   => 'bdc/qun/edit',
                                'url_target'  => '_self',
                                'online_hide' => 0,
                                'sort'        => 100,
                            ],
                            [
                                'title'       => '删除',
                                'icon'        => '',
                                'url_type'    => 'module_admin',
                                'url_value'   => 'bdc/qun/delete',
                                'url_target'  => '_self',
                                'online_hide' => 0,
                                'sort'        => 100,
                            ],
                            [
                                'title'       => '启用',
                                'icon'        => '',
                                'url_type'    => 'module_admin',
                                'url_value'   => 'bdc/qun/enable',
                                'url_target'  => '_self',
                                'online_hide' => 0,
                                'sort'        => 100,
                            ],
                            [
                                'title'       => '禁用',
                                'icon'        => '',
                                'url_type'    => 'module_admin',
                                'url_value'   => 'bdc/qun/disable',
                                'url_target'  => '_self',
                                'online_hide' => 0,
                                'sort'        => 100,
                            ],
                            [
                                'title'       => '快速编辑',
                                'icon'        => '',
                                'url_type'    => 'module_admin',
                                'url_value'   => 'bdc/qun/quickedit',
                                'url_target'  => '_self',
                                'online_hide' => 0,
                                'sort'        => 100,
                            ],
                        ],
                    ],

                    [
                        'title'       => '社群新增',
                        'icon'        => 'fa fa-fw fa-cart-plus',
                        'url_type'    => 'module_admin',
                        'url_value'   => 'bdc/qun/add',
                        'url_target'  => '_self',
                        'online_hide' => 0,
                        'sort'        => 100,
                    ],
                ],
            ],
        ],
    ],
];