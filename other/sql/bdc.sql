/*
Navicat MySQL Data Transfer

Source Server         : moco
Source Server Version : 50556
Source Host           : 101.132.188.167:3306
Source Database       : bdc

Target Server Type    : MYSQL
Target Server Version : 50556
File Encoding         : 65001

Date: 2018-08-06 17:11:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `rs_admin_access`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_access`;
CREATE TABLE `rs_admin_access` (
  `module` varchar(16) NOT NULL DEFAULT '' COMMENT '模型名称',
  `group` varchar(16) NOT NULL DEFAULT '' COMMENT '权限分组标识',
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `nid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '授权节点id'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='统一授权表';

-- ----------------------------
-- Records of rs_admin_access
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_admin_action`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_action`;
CREATE TABLE `rs_admin_action` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(16) NOT NULL DEFAULT '' COMMENT '所属模块名',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '行为唯一标识',
  `title` varchar(80) NOT NULL DEFAULT '' COMMENT '行为标题',
  `remark` varchar(128) NOT NULL DEFAULT '' COMMENT '行为描述',
  `rule` text NOT NULL COMMENT '行为规则',
  `log` text NOT NULL COMMENT '日志规则',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=385 DEFAULT CHARSET=utf8 COMMENT='系统行为表';

-- ----------------------------
-- Records of rs_admin_action
-- ----------------------------
INSERT INTO `rs_admin_action` VALUES ('1', 'user', 'user_add', '添加用户', '添加用户', '', '[user|get_nickname] 添加了用户：[record|get_nickname]', '1', '1480156399', '1480163853');
INSERT INTO `rs_admin_action` VALUES ('2', 'user', 'user_edit', '编辑用户', '编辑用户', '', '[user|get_nickname] 编辑了用户：[details]', '1', '1480164578', '1480297748');
INSERT INTO `rs_admin_action` VALUES ('3', 'user', 'user_delete', '删除用户', '删除用户', '', '[user|get_nickname] 删除了用户：[details]', '1', '1480168582', '1480168616');
INSERT INTO `rs_admin_action` VALUES ('4', 'user', 'user_enable', '启用用户', '启用用户', '', '[user|get_nickname] 启用了用户：[details]', '1', '1480169185', '1480169185');
INSERT INTO `rs_admin_action` VALUES ('5', 'user', 'user_disable', '禁用用户', '禁用用户', '', '[user|get_nickname] 禁用了用户：[details]', '1', '1480169214', '1480170581');
INSERT INTO `rs_admin_action` VALUES ('6', 'user', 'user_access', '用户授权', '用户授权', '', '[user|get_nickname] 对用户：[record|get_nickname] 进行了授权操作。详情：[details]', '1', '1480221441', '1480221563');
INSERT INTO `rs_admin_action` VALUES ('7', 'user', 'role_add', '添加角色', '添加角色', '', '[user|get_nickname] 添加了角色：[details]', '1', '1480251473', '1480251473');
INSERT INTO `rs_admin_action` VALUES ('8', 'user', 'role_edit', '编辑角色', '编辑角色', '', '[user|get_nickname] 编辑了角色：[details]', '1', '1480252369', '1480252369');
INSERT INTO `rs_admin_action` VALUES ('9', 'user', 'role_delete', '删除角色', '删除角色', '', '[user|get_nickname] 删除了角色：[details]', '1', '1480252580', '1480252580');
INSERT INTO `rs_admin_action` VALUES ('10', 'user', 'role_enable', '启用角色', '启用角色', '', '[user|get_nickname] 启用了角色：[details]', '1', '1480252620', '1480252620');
INSERT INTO `rs_admin_action` VALUES ('11', 'user', 'role_disable', '禁用角色', '禁用角色', '', '[user|get_nickname] 禁用了角色：[details]', '1', '1480252651', '1480252651');
INSERT INTO `rs_admin_action` VALUES ('12', 'user', 'attachment_enable', '启用附件', '启用附件', '', '[user|get_nickname] 启用了附件：附件ID([details])', '1', '1480253226', '1480253332');
INSERT INTO `rs_admin_action` VALUES ('13', 'user', 'attachment_disable', '禁用附件', '禁用附件', '', '[user|get_nickname] 禁用了附件：附件ID([details])', '1', '1480253267', '1480253340');
INSERT INTO `rs_admin_action` VALUES ('14', 'user', 'attachment_delete', '删除附件', '删除附件', '', '[user|get_nickname] 删除了附件：附件ID([details])', '1', '1480253323', '1480253323');
INSERT INTO `rs_admin_action` VALUES ('15', 'admin', 'config_add', '添加配置', '添加配置', '', '[user|get_nickname] 添加了配置，[details]', '1', '1480296196', '1480296196');
INSERT INTO `rs_admin_action` VALUES ('16', 'admin', 'config_edit', '编辑配置', '编辑配置', '', '[user|get_nickname] 编辑了配置：[details]', '1', '1480296960', '1480296960');
INSERT INTO `rs_admin_action` VALUES ('17', 'admin', 'config_enable', '启用配置', '启用配置', '', '[user|get_nickname] 启用了配置：[details]', '1', '1480298479', '1480298479');
INSERT INTO `rs_admin_action` VALUES ('18', 'admin', 'config_disable', '禁用配置', '禁用配置', '', '[user|get_nickname] 禁用了配置：[details]', '1', '1480298506', '1480298506');
INSERT INTO `rs_admin_action` VALUES ('19', 'admin', 'config_delete', '删除配置', '删除配置', '', '[user|get_nickname] 删除了配置：[details]', '1', '1480298532', '1480298532');
INSERT INTO `rs_admin_action` VALUES ('20', 'admin', 'database_export', '备份数据库', '备份数据库', '', '[user|get_nickname] 备份了数据库：[details]', '1', '1480298946', '1480298946');
INSERT INTO `rs_admin_action` VALUES ('21', 'admin', 'database_import', '还原数据库', '还原数据库', '', '[user|get_nickname] 还原了数据库：[details]', '1', '1480301990', '1480302022');
INSERT INTO `rs_admin_action` VALUES ('22', 'admin', 'database_optimize', '优化数据表', '优化数据表', '', '[user|get_nickname] 优化了数据表：[details]', '1', '1480302616', '1480302616');
INSERT INTO `rs_admin_action` VALUES ('23', 'admin', 'database_repair', '修复数据表', '修复数据表', '', '[user|get_nickname] 修复了数据表：[details]', '1', '1480302798', '1480302798');
INSERT INTO `rs_admin_action` VALUES ('24', 'admin', 'database_backup_delete', '删除数据库备份', '删除数据库备份', '', '[user|get_nickname] 删除了数据库备份：[details]', '1', '1480302870', '1480302870');
INSERT INTO `rs_admin_action` VALUES ('25', 'admin', 'hook_add', '添加钩子', '添加钩子', '', '[user|get_nickname] 添加了钩子：[details]', '1', '1480303198', '1480303198');
INSERT INTO `rs_admin_action` VALUES ('26', 'admin', 'hook_edit', '编辑钩子', '编辑钩子', '', '[user|get_nickname] 编辑了钩子：[details]', '1', '1480303229', '1480303229');
INSERT INTO `rs_admin_action` VALUES ('27', 'admin', 'hook_delete', '删除钩子', '删除钩子', '', '[user|get_nickname] 删除了钩子：[details]', '1', '1480303264', '1480303264');
INSERT INTO `rs_admin_action` VALUES ('28', 'admin', 'hook_enable', '启用钩子', '启用钩子', '', '[user|get_nickname] 启用了钩子：[details]', '1', '1480303294', '1480303294');
INSERT INTO `rs_admin_action` VALUES ('29', 'admin', 'hook_disable', '禁用钩子', '禁用钩子', '', '[user|get_nickname] 禁用了钩子：[details]', '1', '1480303409', '1480303409');
INSERT INTO `rs_admin_action` VALUES ('30', 'admin', 'menu_add', '添加节点', '添加节点', '', '[user|get_nickname] 添加了节点：[details]', '1', '1480305468', '1480305468');
INSERT INTO `rs_admin_action` VALUES ('31', 'admin', 'menu_edit', '编辑节点', '编辑节点', '', '[user|get_nickname] 编辑了节点：[details]', '1', '1480305513', '1480305513');
INSERT INTO `rs_admin_action` VALUES ('32', 'admin', 'menu_delete', '删除节点', '删除节点', '', '[user|get_nickname] 删除了节点：[details]', '1', '1480305562', '1480305562');
INSERT INTO `rs_admin_action` VALUES ('33', 'admin', 'menu_enable', '启用节点', '启用节点', '', '[user|get_nickname] 启用了节点：[details]', '1', '1480305630', '1480305630');
INSERT INTO `rs_admin_action` VALUES ('34', 'admin', 'menu_disable', '禁用节点', '禁用节点', '', '[user|get_nickname] 禁用了节点：[details]', '1', '1480305659', '1480305659');
INSERT INTO `rs_admin_action` VALUES ('35', 'admin', 'module_install', '安装模块', '安装模块', '', '[user|get_nickname] 安装了模块：[details]', '1', '1480307558', '1480307558');
INSERT INTO `rs_admin_action` VALUES ('36', 'admin', 'module_uninstall', '卸载模块', '卸载模块', '', '[user|get_nickname] 卸载了模块：[details]', '1', '1480307588', '1480307588');
INSERT INTO `rs_admin_action` VALUES ('37', 'admin', 'module_enable', '启用模块', '启用模块', '', '[user|get_nickname] 启用了模块：[details]', '1', '1480307618', '1480307618');
INSERT INTO `rs_admin_action` VALUES ('38', 'admin', 'module_disable', '禁用模块', '禁用模块', '', '[user|get_nickname] 禁用了模块：[details]', '1', '1480307653', '1480307653');
INSERT INTO `rs_admin_action` VALUES ('39', 'admin', 'module_export', '导出模块', '导出模块', '', '[user|get_nickname] 导出了模块：[details]', '1', '1480307682', '1480307682');
INSERT INTO `rs_admin_action` VALUES ('40', 'admin', 'packet_install', '安装数据包', '安装数据包', '', '[user|get_nickname] 安装了数据包：[details]', '1', '1480308342', '1480308342');
INSERT INTO `rs_admin_action` VALUES ('41', 'admin', 'packet_uninstall', '卸载数据包', '卸载数据包', '', '[user|get_nickname] 卸载了数据包：[details]', '1', '1480308372', '1480308372');
INSERT INTO `rs_admin_action` VALUES ('42', 'admin', 'system_config_update', '更新系统设置', '更新系统设置', '', '[user|get_nickname] 更新了系统设置：[details]', '1', '1480309555', '1480309642');

-- ----------------------------
-- Table structure for `rs_admin_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_attachment`;
CREATE TABLE `rs_admin_attachment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '用户id',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '文件名',
  `module` varchar(32) NOT NULL DEFAULT '' COMMENT '模块名，由哪个模块上传的',
  `path` varchar(255) NOT NULL DEFAULT '' COMMENT '文件路径',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图路径',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '文件链接',
  `mime` varchar(128) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `ext` char(8) NOT NULL DEFAULT '' COMMENT '文件类型',
  `size` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `md5` char(32) NOT NULL DEFAULT '' COMMENT '文件md5',
  `sha1` char(40) NOT NULL DEFAULT '' COMMENT 'sha1 散列值',
  `driver` varchar(16) NOT NULL DEFAULT 'local' COMMENT '上传驱动',
  `download` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '下载次数',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上传时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `width` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片宽度',
  `height` int(8) unsigned NOT NULL DEFAULT '0' COMMENT '图片高度',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
-- Records of rs_admin_attachment
-- ----------------------------
INSERT INTO `rs_admin_attachment` VALUES ('1', '1', 'pic3.JPG', 'bdc', 'uploads/images/20180729/a261f8df3b8f88a0d0ba5c1a592027a0.JPG', '', '', 'image/jpeg', 'JPG', '21294', '30ecd6bc91d213e19ada6e9ae3c5b445', 'e58df706bebcc372fa80181c0d760a1cb2f1de41', 'local', '0', '1532835076', '1532835076', '100', '1', '640', '640');
INSERT INTO `rs_admin_attachment` VALUES ('2', '1', 'icon.jpg', 'bdc', 'uploads/images/20180731/40df1f020e2bf761d4312364bc1cc1f0.jpg', '', '', 'image/jpeg', 'jpg', '184534', '9e899c0926a2c7300501a124e8f035c9', '0c349b24e3adb3d79c2855a61e430abdc5f9b8d2', 'local', '0', '1532971572', '1532971572', '100', '1', '640', '640');
INSERT INTO `rs_admin_attachment` VALUES ('3', '1', '1.png', 'bdc', 'uploads/images/20180801/2114fceb39c98bbfa9bda27a7bf62caa.png', '', '', 'image/png', 'png', '93985', '0c132ebdf4cceeb5210c503178ec18b3', '006d94bc7d57f3b5531879bc0b093278ecc44ed1', 'local', '0', '1533099129', '1533099129', '100', '1', '357', '334');
INSERT INTO `rs_admin_attachment` VALUES ('4', '1', '2.png', 'bdc', 'uploads/images/20180801/c44fe44dad1cb8388785719567a86c1f.png', '', '', 'image/png', 'png', '166881', '9fd76ea322709978f8f1d86d496601b9', 'e5579ed85669b58804e095d0531526b9a3d9adb4', 'local', '0', '1533099377', '1533099377', '100', '1', '472', '457');
INSERT INTO `rs_admin_attachment` VALUES ('5', '1', '3.png', 'bdc', 'uploads/images/20180801/e639884a04df78fd91e3d72bca45af8f.png', '', '', 'image/png', 'png', '156516', 'c00d55847b8cdfabe91e9c74832290e9', '58079256e4dafc8ff4a7f77a993820241cf86e8c', 'local', '0', '1533099544', '1533099544', '100', '1', '347', '346');
INSERT INTO `rs_admin_attachment` VALUES ('6', '1', '4.png', 'bdc', 'uploads/images/20180801/1158d74c329c3818b18c7851ce0bb5f0.png', '', '', 'image/png', 'png', '103244', 'f07e8be6865f91a9950100f5d1030966', 'a90665835f391b29e647f236694d4fa6b265f770', 'local', '0', '1533099969', '1533099969', '100', '1', '351', '355');
INSERT INTO `rs_admin_attachment` VALUES ('7', '1', '5.png', 'bdc', 'uploads/images/20180801/ffbfd659ab31887f3cf4112655d80e4e.png', '', '', 'image/png', 'png', '230953', '6d9e0ea0e31b521525a805039085384e', '75bae11c48c7adcd1ed4e51d98383f569c1b328b', 'local', '0', '1533100060', '1533100060', '100', '1', '349', '343');
INSERT INTO `rs_admin_attachment` VALUES ('8', '1', '6.png', 'bdc', 'uploads/images/20180801/ef7c036c393526948753f9fbf0477aa2.png', '', '', 'image/png', 'png', '286000', 'f6b140fb4951a47333557ecb1b554234', 'e4b981adc04ab535fce0f43e491f61d9099a8067', 'local', '0', '1533112694', '1533112694', '100', '1', '346', '348');
INSERT INTO `rs_admin_attachment` VALUES ('9', '1', '7.png', 'bdc', 'uploads/images/20180801/6b1e9ed5bb6790e21e83dcc82d369997.png', '', '', 'image/png', 'png', '168715', '1214d83f635bdaec5d5077f9f5e70e7d', 'c930b58ac666a9245967100a8ba2e2612b71d5c5', 'local', '0', '1533112761', '1533112761', '100', '1', '300', '297');
INSERT INTO `rs_admin_attachment` VALUES ('10', '1', '8.png', 'bdc', 'uploads/images/20180801/c9473128f39f212ad4092b957bb44bb0.png', '', '', 'image/png', 'png', '26474', '4ea782a40daed0f24350891903b95606', '8b752e5c3410bef07045455d2e81ca29624954c5', 'local', '0', '1533112986', '1533112986', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('11', '1', '9.png', 'bdc', 'uploads/images/20180801/ebd540e15a1992c70fafbb4cb336f673.png', '', '', 'image/png', 'png', '52104', 'b14dfe2a9481fb40d797f954bef28282', '3539d71270964be10b362243df2d1ad507abc511', 'local', '0', '1533113260', '1533113260', '100', '1', '348', '346');
INSERT INTO `rs_admin_attachment` VALUES ('12', '1', '10.png', 'bdc', 'uploads/images/20180801/5a78f6549176a8d48ca8e4d6cfe855fa.png', '', '', 'image/png', 'png', '66906', '92a38c4a988e37cf04544f7e7f32869a', '57e62d3d62b67544a7849e46904eed4ace79556f', 'local', '0', '1533113365', '1533113365', '100', '1', '201', '198');
INSERT INTO `rs_admin_attachment` VALUES ('13', '1', '11.png', 'bdc', 'uploads/images/20180801/4b4a4471a0a7033df7f69c78e3f7a8f3.png', '', '', 'image/png', 'png', '31044', 'bc053b817db958c5c0ccbed9ca787e51', '3fb4e89802803fecc11d69f7f48b0e00cb38647b', 'local', '0', '1533113442', '1533113442', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('14', '1', '12.png', 'bdc', 'uploads/images/20180801/ea78d357f2ee7da70a0be78f4564fa91.png', '', '', 'image/png', 'png', '206230', '0db0856001aebd122e3dd23ad7d374dd', '8aa9aa4eb10b0cad7c79bc4b0aa5f56bb419053d', 'local', '0', '1533113511', '1533113511', '100', '1', '347', '346');
INSERT INTO `rs_admin_attachment` VALUES ('15', '1', '13.png', 'bdc', 'uploads/images/20180801/d199b8c3b90c2b2fbd3f762ab4bdc193.png', '', '', 'image/png', 'png', '72323', '509cbd60bdbe31ae6c95180a39f7c134', 'b3ab983644d187425f3965286953c67472a9529f', 'local', '0', '1533113574', '1533113574', '100', '1', '198', '198');
INSERT INTO `rs_admin_attachment` VALUES ('16', '1', '14.png', 'bdc', 'uploads/images/20180801/28d2b563db96fd1ac26b6e0ca59878ea.png', '', '', 'image/png', 'png', '105435', '5321db26f8edbf6a438440e2bed2895b', 'a89301c131bfb6a15e07749192f5824fde3b958e', 'local', '0', '1533113662', '1533113662', '100', '1', '342', '347');
INSERT INTO `rs_admin_attachment` VALUES ('17', '1', '15.png', 'bdc', 'uploads/images/20180801/0478af5689e0434b49a226a0e49b7741.png', '', '', 'image/png', 'png', '52542', '39d6e3d4c04513f0b620d1cc3c7746ee', '1e2d906ee945a7831b6ed147b92963407db35f19', 'local', '0', '1533114076', '1533114076', '100', '1', '343', '343');
INSERT INTO `rs_admin_attachment` VALUES ('18', '1', '16.png', 'bdc', 'uploads/images/20180801/ea9a0a6cd26998203d5e758efeeec40f.png', '', '', 'image/png', 'png', '62581', '84ffa18a2048a1e45f736780438e77f8', '977a8ea68368bcd28b9077f24534c5236880430e', 'local', '0', '1533114204', '1533114204', '100', '1', '194', '193');
INSERT INTO `rs_admin_attachment` VALUES ('19', '1', '17.png', 'bdc', 'uploads/images/20180803/430efad9e73592e14ce8fa2566550aa0.png', '', '', 'image/png', 'png', '121356', '7f0c57317ac98f078c86f0cad1f3adee', '78a82091a1dae45abf7e1f2989e35e55416475be', 'local', '0', '1533263093', '1533263093', '100', '1', '395', '397');
INSERT INTO `rs_admin_attachment` VALUES ('20', '1', '18.png', 'bdc', 'uploads/images/20180803/50294f16074492fc7ee46116ecdddd9f.png', '', '', 'image/png', 'png', '39804', 'b06f8d584ddcb02fa20c33cb4916ea9b', '4d2a10caecdb25a36f51d8e8f24290b78be53f23', 'local', '0', '1533263253', '1533263253', '100', '1', '347', '345');
INSERT INTO `rs_admin_attachment` VALUES ('21', '1', '19.png', 'bdc', 'uploads/images/20180803/624a728ded88f203bd3a4feba5ccef64.png', '', '', 'image/png', 'png', '81792', '3c1b3531068a1d87691563dbe86cd04a', '23087072852696841b10f2089889b64837151706', 'local', '0', '1533263361', '1533263361', '100', '1', '345', '346');
INSERT INTO `rs_admin_attachment` VALUES ('22', '1', '20.png', 'bdc', 'uploads/images/20180803/d4c2afcc8c0574bc0925f687626c4506.png', '', '', 'image/png', 'png', '149708', 'a024247ae63459b73c940330e89b545e', '4d4f06a0cc8703e02544e6034ae434e149c751fe', 'local', '0', '1533263730', '1533263730', '100', '1', '393', '350');
INSERT INTO `rs_admin_attachment` VALUES ('23', '1', '21.png', 'bdc', 'uploads/images/20180803/e53819b4fab1b9ac493ac788bd6af11e.png', '', '', 'image/png', 'png', '96021', 'a2cf44b551870db8792bea9da3d1806a', 'dbdf7673ee2bebee77e4ccaacbe3f9fd714cc932', 'local', '0', '1533264069', '1533264069', '100', '1', '239', '239');
INSERT INTO `rs_admin_attachment` VALUES ('24', '1', '22.png', 'bdc', 'uploads/images/20180803/a64554bb3ec16b2a579afebb3e23e294.png', '', '', 'image/png', 'png', '173365', 'd9679e11641c3d008f53d254362acb90', '711abd3260e979002ac45812fc5b9a45ef06a987', 'local', '0', '1533264231', '1533264231', '100', '1', '361', '350');
INSERT INTO `rs_admin_attachment` VALUES ('25', '1', '23.png', 'bdc', 'uploads/images/20180803/3dc34e004037d47d7914e1e00c6af0a2.png', '', '', 'image/png', 'png', '177305', '16b35ebccabd8d5c7c419be0daaf2088', '59c33ee456fe2ea1060ee07d4302628c9c7d0163', 'local', '0', '1533264675', '1533264675', '100', '1', '345', '341');
INSERT INTO `rs_admin_attachment` VALUES ('26', '1', '24.png', 'bdc', 'uploads/images/20180803/e357634dc79693e621fc7b2362cab527.png', '', '', 'image/png', 'png', '157106', '3ba328d67ce07c17c3e5c64d8f57c9bf', 'ff092bf59b94a28837ff73bb89fb551417d70682', 'local', '0', '1533264865', '1533264865', '100', '1', '348', '343');
INSERT INTO `rs_admin_attachment` VALUES ('27', '1', '25.png', 'bdc', 'uploads/images/20180803/95c3ae1a617b1bba27e55c01d6627587.png', '', '', 'image/png', 'png', '196703', '47611528fde038fd87c6ad505a29a822', '37aad1ec5141cbaab63e10fed467d1f56d31f498', 'local', '0', '1533265029', '1533265029', '100', '1', '342', '338');
INSERT INTO `rs_admin_attachment` VALUES ('28', '1', '26.png', 'bdc', 'uploads/images/20180803/d0f2ed41bbe6b8c2559a920cd1cecd95.png', '', '', 'image/png', 'png', '70290', '42234b0c5d4c69bf0179f007b66713da', '18735937ed43c4a2f3d3a73eb6e893d5b5be6d9d', 'local', '0', '1533265445', '1533265445', '100', '1', '198', '200');
INSERT INTO `rs_admin_attachment` VALUES ('29', '1', '27.png', 'bdc', 'uploads/images/20180803/bda11c52e4b02e260c965a72ecccfb74.png', '', '', 'image/png', 'png', '18215', '27c6f47b21d9bc5c68769f029a235e7e', '540f537a03294cffae2745448356580999852fa1', 'local', '0', '1533265564', '1533265564', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('30', '1', '28.png', 'bdc', 'uploads/images/20180803/d655acea948076a1a01bd4244ff1590c.png', '', '', 'image/png', 'png', '105315', '111867ed3f733dee747ea781522a6a08', 'ff9fb23d5307504be8c15c91f815abbc5a6fef84', 'local', '0', '1533265798', '1533265798', '100', '1', '341', '350');
INSERT INTO `rs_admin_attachment` VALUES ('31', '1', '29.png', 'bdc', 'uploads/images/20180803/36c5f18fc5e6fa214a242ee99d36f8d2.png', '', '', 'image/png', 'png', '79270', '61ead336baac949e70320fd8682aa2b3', '3cf48c6573ed6a91a271c66bba5e35118964c5e0', 'local', '0', '1533265946', '1533265946', '100', '1', '324', '328');
INSERT INTO `rs_admin_attachment` VALUES ('32', '1', '30.png', 'bdc', 'uploads/images/20180803/075f83d8cf77d7adb4646f94eb4c792b.png', '', '', 'image/png', 'png', '66291', '33c39b4bba45c979c8bc0510346c1f00', '395ab39daf281be8554321880f2c4ec409ac33ce', 'local', '0', '1533266217', '1533266217', '100', '1', '199', '200');
INSERT INTO `rs_admin_attachment` VALUES ('33', '1', '31.png', 'bdc', 'uploads/images/20180803/6b43648e0de353e321382297198c94b9.png', '', '', 'image/png', 'png', '34275', 'ecf7687e613043b0555a63703871093d', '1308c3d2bfdd473f10441a6ec130a078f6bce969', 'local', '0', '1533266375', '1533266375', '100', '1', '202', '199');
INSERT INTO `rs_admin_attachment` VALUES ('34', '1', '32.png', 'bdc', 'uploads/images/20180803/cd6f1970b50377e5a424d640dd1ad183.png', '', '', 'image/png', 'png', '127502', '43a6e4fa123142681c67a7e4f1edf096', 'f2f8bc34d80b7d8ef21278b83573b007ee88f6a4', 'local', '0', '1533266485', '1533266485', '100', '1', '351', '344');
INSERT INTO `rs_admin_attachment` VALUES ('35', '1', '33.png', 'bdc', 'uploads/images/20180803/7faf50962b04814713f149322f88b0f8.png', '', '', 'image/png', 'png', '70631', 'c1cc8e7292c18e58dbfae606ce034fc9', 'd0127444b58149a96ff8ade61a96c699f92b7076', 'local', '0', '1533266596', '1533266596', '100', '1', '223', '220');
INSERT INTO `rs_admin_attachment` VALUES ('36', '1', '34.png', 'bdc', 'uploads/images/20180803/fac0e9070c871570853b5073749d64ed.png', '', '', 'image/png', 'png', '91369', '987e437b2c03b0c0ecd3e540f99897ef', '40c6d513f6a3d943fcc65baad3b5b74a1bdfc2d1', 'local', '0', '1533266754', '1533266754', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('37', '1', '35.png', 'bdc', 'uploads/images/20180803/b3b6c20f247e23f7e7c659a8fa268cef.png', '', '', 'image/png', 'png', '70645', 'd44be09d92901766e46ac9430d7fb2e3', 'd33d117201f01985b0c71a184f4ca0bf54f8d2f6', 'local', '0', '1533266892', '1533266892', '100', '1', '201', '200');
INSERT INTO `rs_admin_attachment` VALUES ('38', '1', '36.png', 'bdc', 'uploads/images/20180803/8cbbeeca9ecb6ac093063c639a8e9482.png', '', '', 'image/png', 'png', '128773', '99ca0bd1c643744cd4111c833e269ca8', '331471f9284aabd79414ddad2ac3cb7b41bffb45', 'local', '0', '1533268487', '1533268487', '100', '1', '338', '350');
INSERT INTO `rs_admin_attachment` VALUES ('39', '1', '37.png', 'bdc', 'uploads/images/20180803/0ffeae392e268f447176d1c6b4e0e638.png', '', '', 'image/png', 'png', '111649', '4779b0a4df463384e23bff52dc5fd522', '738f6e0e07eac18d92686cf806473b945b577b4e', 'local', '0', '1533268600', '1533268600', '100', '1', '300', '298');
INSERT INTO `rs_admin_attachment` VALUES ('40', '1', '38.png', 'bdc', 'uploads/images/20180803/87938881a2a293aaecd9f4c38a93716a.png', '', '', 'image/png', 'png', '73845', 'b29ce31958353327bc8753be5c5ad3e5', 'a5a477e1e8f70fa5b07570700fc55786cadb6c0f', 'local', '0', '1533268712', '1533268712', '100', '1', '223', '223');
INSERT INTO `rs_admin_attachment` VALUES ('41', '1', '39.png', 'bdc', 'uploads/images/20180803/bb35eb0127ecd7cc6202760b0e3ccf92.png', '', '', 'image/png', 'png', '77042', 'c9ae0a68cc0c44c15f147c268d8ee003', 'ca35e527ada39ee7706cfb2951cfb0119f519309', 'local', '0', '1533268845', '1533268845', '100', '1', '202', '201');
INSERT INTO `rs_admin_attachment` VALUES ('42', '1', '40.png', 'bdc', 'uploads/images/20180803/382815f70d896d80f2a4bdaa3f991731.png', '', '', 'image/png', 'png', '36326', '0df10e412213311743450259e3e76d9d', '809068045ba01ab748add5ac9c507d338702c676', 'local', '0', '1533269010', '1533269010', '100', '1', '195', '196');
INSERT INTO `rs_admin_attachment` VALUES ('43', '1', '41.png', 'bdc', 'uploads/images/20180803/f6f6b1f34937fdd3c219ed6a09b42b32.png', '', '', 'image/png', 'png', '87222', 'fc92bf2a4853a90276df1e0e836b92ad', 'a4a29c08971fcc998a9aac0e5e80b372b342f514', 'local', '0', '1533269151', '1533269151', '100', '1', '198', '199');
INSERT INTO `rs_admin_attachment` VALUES ('44', '1', '42.png', 'bdc', 'uploads/images/20180803/943dcfa80e13a92a48861dbece3b6ef0.png', '', '', 'image/png', 'png', '112571', '21a2c37dcb2eb01357c2241338b81d5e', '7551ad4f11cd7922b446a7d4cc9bca440707c34a', 'local', '0', '1533269254', '1533269254', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('45', '1', '43.png', 'bdc', 'uploads/images/20180803/0d89ad84b1698be4580e70975b386b2c.png', '', '', 'image/png', 'png', '63843', '221e83d3189a1a471c9a290d80dab79c', 'b584c45e0f260c128144596f04bd490882624f08', 'local', '0', '1533269388', '1533269388', '100', '1', '202', '202');
INSERT INTO `rs_admin_attachment` VALUES ('46', '1', '44.png', 'bdc', 'uploads/images/20180803/e5b1a0d2137e769a913e28205d209070.png', '', '', 'image/png', 'png', '166115', '4ef1043aa2a072ed6290af67b8b62998', 'e392d358057902cadc9f6bd61b861c7be1fad893', 'local', '0', '1533269500', '1533269500', '100', '1', '345', '343');
INSERT INTO `rs_admin_attachment` VALUES ('47', '1', '45.png', 'bdc', 'uploads/images/20180803/642d8c849432d071423cb6715cbe22d4.png', '', '', 'image/png', 'png', '157306', '0de2d93577598578fb830a99620ba660', 'c92b6d8e9453ad5dc7621646334fe2375829a52a', 'local', '0', '1533269591', '1533269591', '100', '1', '341', '347');
INSERT INTO `rs_admin_attachment` VALUES ('48', '1', '46.png', 'bdc', 'uploads/images/20180803/7e4032040114295c36359d06ebf96ff6.png', '', '', 'image/png', 'png', '143552', 'a7c0034bdb47d51c797d7d46e93ea55a', '12863680c0580beb130a29a3134a157507b4aaaa', 'local', '0', '1533269671', '1533269671', '100', '1', '315', '299');
INSERT INTO `rs_admin_attachment` VALUES ('49', '1', '47.png', 'bdc', 'uploads/images/20180803/b207791056ef6e0f82c9c15616c1c4c7.png', '', '', 'image/png', 'png', '76259', '1b2d54c104b6f589e9e7c271278b0591', '3f6ee879b168d9b5c745ced7a29aa38df4b0ea8d', 'local', '0', '1533270418', '1533270418', '100', '1', '200', '196');
INSERT INTO `rs_admin_attachment` VALUES ('50', '1', '48.png', 'bdc', 'uploads/images/20180803/4c514388259daa45291ffca1010da69e.png', '', '', 'image/png', 'png', '208919', '4f01ee00fbd8af367f68101b0fc4d27f', '3f871cb6d8b804d65f48701742b135f67d59b653', 'local', '0', '1533270526', '1533270526', '100', '1', '382', '351');
INSERT INTO `rs_admin_attachment` VALUES ('51', '1', '49.png', 'bdc', 'uploads/images/20180803/148a9fb0775471f1a718cf789b6c7061.png', '', '', 'image/png', 'png', '50341', 'eed4b5789685034b4fb8e2098cb7c9af', '303ace052e3a2b33958608057073db8a59ab1dc0', 'local', '0', '1533270666', '1533270666', '100', '1', '347', '348');
INSERT INTO `rs_admin_attachment` VALUES ('52', '1', '50.png', 'bdc', 'uploads/images/20180803/e39ed67a8832c5a447949de5a5281687.png', '', '', 'image/png', 'png', '88658', 'ff2f1c066e6951374d5559ee69e2a97d', '69fac297eacec9483bfa226f99dcc957be6bd8fd', 'local', '0', '1533270774', '1533270774', '100', '1', '341', '349');
INSERT INTO `rs_admin_attachment` VALUES ('53', '1', '51.png', 'bdc', 'uploads/images/20180803/46ed8ae91546460c96ca9dd7fad3c308.png', '', '', 'image/png', 'png', '80232', '0c4583160a81948d72920cf97fa66951', '5beaf5dbdd2a96bf9b84487bffd328977a478458', 'local', '0', '1533270894', '1533270894', '100', '1', '289', '294');
INSERT INTO `rs_admin_attachment` VALUES ('54', '1', '52.png', 'bdc', 'uploads/images/20180803/606cf090daf93c35561b265b36adfd70.png', '', '', 'image/png', 'png', '210382', '513a7e8a45b53151308664d8c3904672', 'f7d40b6eee9a1819b5311cf7a5e546b9940c8bbb', 'local', '0', '1533271106', '1533271106', '100', '1', '327', '321');
INSERT INTO `rs_admin_attachment` VALUES ('55', '1', '53.png', 'bdc', 'uploads/images/20180803/f8d9256510a7d33056cc5329bb9829aa.png', '', '', 'image/png', 'png', '341414', '45778d08c2e2f6690967ce1a7689fd1e', '8e2630a58590604289fe969f3c036eb3d46cc646', 'local', '0', '1533271217', '1533271217', '100', '1', '346', '345');
INSERT INTO `rs_admin_attachment` VALUES ('56', '1', '54.png', 'bdc', 'uploads/images/20180803/441711d4ee98820afca64fca1c2c0507.png', '', '', 'image/png', 'png', '69564', 'dca1c1178738f37c581c72e9a19e14db', '7486f0f37b06589a042937e2213bac039efc063d', 'local', '0', '1533272998', '1533272998', '100', '1', '200', '201');
INSERT INTO `rs_admin_attachment` VALUES ('57', '1', '55.png', 'bdc', 'uploads/images/20180803/ef60d697df7ca2d97840bfd3ab5100fd.png', '', '', 'image/png', 'png', '117880', '972e5de8e4891d54ac65f6170485d58d', '5f127cb962f8d13087024ccd84ab883fc4f79de1', 'local', '0', '1533273071', '1533273071', '100', '1', '339', '382');
INSERT INTO `rs_admin_attachment` VALUES ('58', '1', '56.png', 'bdc', 'uploads/images/20180803/20869a36475afd88e3ea7dd516093696.png', '', '', 'image/png', 'png', '79110', 'cb7277fcc0d88c457b8998d5062455e8', '9e404b179e1cba9b1fec2bba08921e59b7dfa604', 'local', '0', '1533273159', '1533273159', '100', '1', '200', '201');
INSERT INTO `rs_admin_attachment` VALUES ('59', '1', '57.png', 'bdc', 'uploads/images/20180803/bb8d8b6dfc0ec0686cdd0228b1dd6580.png', '', '', 'image/png', 'png', '63323', 'b4277c528f77bcab29195581d8584894', '652b8ab863cdae298985a83e52f73836386e624d', 'local', '0', '1533273266', '1533273266', '100', '1', '198', '200');
INSERT INTO `rs_admin_attachment` VALUES ('60', '1', '58.png', 'bdc', 'uploads/images/20180803/3bb9aad51bc12083da250e95e5c03bf9.png', '', '', 'image/png', 'png', '161697', '74aa702288936362534981cbc622393b', 'f5cd254481e7453c5615d6641459cf9ce2a9d978', 'local', '0', '1533273369', '1533273369', '100', '1', '339', '344');
INSERT INTO `rs_admin_attachment` VALUES ('61', '1', '59.png', 'bdc', 'uploads/images/20180803/dc5834ceed0fa84c18347382479148c8.png', '', '', 'image/png', 'png', '146181', '43a9bf6ea628e0d11a83295151668949', '262a834e3a1203aea7dddd7507cd33bbca0bb9a5', 'local', '0', '1533273480', '1533273480', '100', '1', '295', '298');
INSERT INTO `rs_admin_attachment` VALUES ('62', '1', 'webwxgetmsgimg (3).jpg', 'bdc', 'uploads/images/20180803/99939512ef72b48c338d8b7854769aae.jpg', '', '', 'image/jpeg', 'jpg', '4976', '41c2f43eb99c72cdc8f743d516b509aa', '5d4c9ccf9dfa03771f42c96f30ef7339aee7a79c', 'local', '0', '1533273601', '1533273601', '100', '1', '120', '120');
INSERT INTO `rs_admin_attachment` VALUES ('63', '1', '60.png', 'bdc', 'uploads/images/20180803/7840c7837ca8889cb6c9d7b35a35b38a.png', '', '', 'image/png', 'png', '76423', 'cf25ff75a112abac25a9d9e922d15d15', '3856360e9037fa79e44260200d8d8c44d45d8ca1', 'local', '0', '1533273824', '1533273824', '100', '1', '342', '333');
INSERT INTO `rs_admin_attachment` VALUES ('64', '1', '61.png', 'bdc', 'uploads/images/20180803/616d1560779d307524fb8e993b798413.png', '', '', 'image/png', 'png', '92361', '3c27d3c919ff25dd0c70a896aa010fc5', 'c69948d071516622194fafa669e74f2553e98443', 'local', '0', '1533273932', '1533273932', '100', '1', '199', '200');
INSERT INTO `rs_admin_attachment` VALUES ('65', '1', '62.png', 'bdc', 'uploads/images/20180803/5fda82425978b8d103b1d3010fc13bb9.png', '', '', 'image/png', 'png', '60513', 'd674c90b648cfeb8c238aab1255fbf22', '60e0e4ea7d1d4a9323a77ca4921b1e64cd73d8bb', 'local', '0', '1533274021', '1533274021', '100', '1', '197', '198');
INSERT INTO `rs_admin_attachment` VALUES ('66', '1', '63.png', 'bdc', 'uploads/images/20180803/cbf550b402ac50719524929ea2823809.png', '', '', 'image/png', 'png', '68361', '93271101dbc7a649f6f66abcd97f397a', '25bcbd4c96ecc074a64bb3279686135538940fcc', 'local', '0', '1533274101', '1533274101', '100', '1', '200', '199');
INSERT INTO `rs_admin_attachment` VALUES ('67', '1', '64.png', 'bdc', 'uploads/images/20180803/34803dc34652c2734d594f56e19f1330.png', '', '', 'image/png', 'png', '191146', 'dbf5160c5e13a7a3a7037858cbf9e435', '34b4406c387028f9f367fbc1f5bf9b2598f8075a', 'local', '0', '1533274212', '1533274212', '100', '1', '445', '441');
INSERT INTO `rs_admin_attachment` VALUES ('68', '1', '65.png', 'bdc', 'uploads/images/20180803/7ed52112e4b2190386e9db19e78f62c6.png', '', '', 'image/png', 'png', '38614', 'e4cd5afaea5990c98059bf38b991e477', 'e1c6c583ee4389fdbb6cbc69da72f2b4520b7f40', 'local', '0', '1533274324', '1533274324', '100', '1', '200', '199');
INSERT INTO `rs_admin_attachment` VALUES ('69', '1', '66.png', 'bdc', 'uploads/images/20180803/4321a7120853f4bf2f19202c9147a0aa.png', '', '', 'image/png', 'png', '38523', 'e5fadfb54f1b4839eade42c53c45fba9', 'f844e94e2157dfc5d3c39b63888da2419e43d847', 'local', '0', '1533274420', '1533274420', '100', '1', '225', '214');
INSERT INTO `rs_admin_attachment` VALUES ('70', '1', '67.png', 'bdc', 'uploads/images/20180803/cfd2fbddf55a8d877eb9cf31f18b51f6.png', '', '', 'image/png', 'png', '72485', '184047167128895052f977db6acd83c0', '17c71f859e424f58a86ef90a089662caf62a1030', 'local', '0', '1533274543', '1533274543', '100', '1', '201', '197');
INSERT INTO `rs_admin_attachment` VALUES ('71', '1', '68.png', 'bdc', 'uploads/images/20180803/530266293fbddf97869a88c8ad634246.png', '', '', 'image/png', 'png', '29424', '1aa10b379a14c21f88eee6ea555fe07a', '0c5b48a99c8d721727a1c852279b651e5cd83155', 'local', '0', '1533274652', '1533274652', '100', '1', '237', '233');
INSERT INTO `rs_admin_attachment` VALUES ('72', '1', '69.png', 'bdc', 'uploads/images/20180803/dfd28492438c869c223c87f9352bda79.png', '', '', 'image/png', 'png', '76389', '7c8c22efb4cb5641a78e9a9a18d07320', '971a77da093b268435fd0274218006aa385d017c', 'local', '0', '1533276988', '1533276988', '100', '1', '197', '197');
INSERT INTO `rs_admin_attachment` VALUES ('73', '1', '70.png', 'bdc', 'uploads/images/20180803/61c725c3e96c201ce212410c69700fc8.png', '', '', 'image/png', 'png', '107614', '038af49b9e27d28c73b52264ee6502cd', '11a17e39480bec14d254d3a058ddca0a7e717ecd', 'local', '0', '1533277105', '1533277105', '100', '1', '313', '330');
INSERT INTO `rs_admin_attachment` VALUES ('74', '1', '71.png', 'bdc', 'uploads/images/20180803/0f2cb81e0ca8fa648200069b7655f25f.png', '', '', 'image/png', 'png', '139750', '15c47236ab05624830e9165bf3b6d158', 'a8523e5c47b067b5a546d1c5959b53b7d1ec3497', 'local', '0', '1533277224', '1533277224', '100', '1', '339', '350');
INSERT INTO `rs_admin_attachment` VALUES ('75', '1', '72.png', 'bdc', 'uploads/images/20180803/0bb7343299291e3e91fb75bd0d947330.png', '', '', 'image/png', 'png', '106924', 'edef3e638045c5747ffda976fb976eb4', 'e50cf3b7c01b231678f8a761b25814e9b8d6f6f7', 'local', '0', '1533277385', '1533277385', '100', '1', '300', '299');
INSERT INTO `rs_admin_attachment` VALUES ('76', '1', '73.png', 'bdc', 'uploads/images/20180803/84639149f6d4311c765b3568e10042a3.png', '', '', 'image/png', 'png', '88618', '54c685945859bff516af65d41197bc8e', '2ea4a8d7ff0d031032eedb25a6c1414c5c62249f', 'local', '0', '1533277567', '1533277567', '100', '1', '302', '292');
INSERT INTO `rs_admin_attachment` VALUES ('77', '1', '74.png', 'bdc', 'uploads/images/20180803/82ec385b1d89e61c49b0a85a0f8615bf.png', '', '', 'image/png', 'png', '74898', 'd6dc07d5d6791ff400b5dbc865b77c93', 'e9c0b4cb7436333532560f74043a36b710b8c8a3', 'local', '0', '1533277722', '1533277722', '100', '1', '329', '360');
INSERT INTO `rs_admin_attachment` VALUES ('78', '1', '75.png', 'bdc', 'uploads/images/20180803/aa9629252f9c6cba74e2b7c129be7dbf.png', '', '', 'image/png', 'png', '232270', '8e741b71de6df0eee147d8984cc7a0a4', 'c1a595a2494ce7ac9f207e3d298723fe026deea3', 'local', '0', '1533277822', '1533277822', '100', '1', '345', '346');
INSERT INTO `rs_admin_attachment` VALUES ('79', '1', '76.png', 'bdc', 'uploads/images/20180803/90d1070c15edb072d604656d31ab4e44.png', '', '', 'image/png', 'png', '67044', '82ed6c1f49e23bf416bbcaa8cf0ca728', '3c28ab849a19a06ffdb3fd6136bb99c6622c1163', 'local', '0', '1533278122', '1533278122', '100', '1', '216', '211');
INSERT INTO `rs_admin_attachment` VALUES ('80', '1', '77.png', 'bdc', 'uploads/images/20180803/37deb65e705b701d1b7e1f65b74356d7.png', '', '', 'image/png', 'png', '103105', '9d2577a461970755f8a25b47badc9f1f', '73a9abcde2bd0750c16874925e620f9a4d59c76e', 'local', '0', '1533278260', '1533278260', '100', '1', '302', '300');
INSERT INTO `rs_admin_attachment` VALUES ('81', '1', '78.png', 'bdc', 'uploads/images/20180803/ebda82a0c4f4d327bf8aafec7411d36b.png', '', '', 'image/png', 'png', '77180', 'aae8d749593117d107413365fd9618e3', '0c4f5ae11c140c891ed82e0e479a97f3b40a49a1', 'local', '0', '1533278364', '1533278364', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('82', '1', '79.png', 'bdc', 'uploads/images/20180803/9ed07b88e7373d53d999e1fcb92f4f90.png', '', '', 'image/png', 'png', '39443', 'fa40810619599f12371c33364f791712', 'f869f922a6f73a3573bdac8f135393613f56d742', 'local', '0', '1533278471', '1533278471', '100', '1', '203', '203');
INSERT INTO `rs_admin_attachment` VALUES ('83', '1', '80.png', 'bdc', 'uploads/images/20180803/a1480d87b6ae225a6d56343edb285ac0.png', '', '', 'image/png', 'png', '61489', 'c386923a1835dc433b85eb2279a48f42', 'c5194424d42c9eebaa1d6833bae58384f0ba4a3e', 'local', '0', '1533278667', '1533278667', '100', '1', '201', '197');
INSERT INTO `rs_admin_attachment` VALUES ('84', '1', '81.png', 'bdc', 'uploads/images/20180803/048f6431408ad6bf8100bea4bf59a278.png', '', '', 'image/png', 'png', '46299', 'db88d0b45c1f0f80e4c00a0349f64c08', 'b8e3aa47a3a8428a23738e747b027464f7195cba', 'local', '0', '1533278782', '1533278782', '100', '1', '197', '197');
INSERT INTO `rs_admin_attachment` VALUES ('85', '1', '82.png', 'bdc', 'uploads/images/20180803/29b6a03e356f523345b81439bb1f0ef0.png', '', '', 'image/png', 'png', '73235', '61f525d95eb94404305aa60fad494a37', 'b70d6990ebe366a311fb5f5f050a8b4cded09b0c', 'local', '0', '1533278928', '1533278928', '100', '1', '199', '198');
INSERT INTO `rs_admin_attachment` VALUES ('86', '1', '83.png', 'bdc', 'uploads/images/20180803/c5472f589a0cfdb5e55468175faab3c1.png', '', '', 'image/png', 'png', '45796', '3e6e5290854d7d8c43ab08893cbf3f1f', '80cd49ea01cbcc404450f2a9c0c95ec749f32795', 'local', '0', '1533279029', '1533279029', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('87', '1', '84.png', 'bdc', 'uploads/images/20180803/66e04bb8c353c503f8fbe34c03728073.png', '', '', 'image/png', 'png', '24196', 'b987045feff7a3a53198a076bb451123', '48edca281167a22bbc63c4ca2de97b05a77e748b', 'local', '0', '1533279140', '1533279140', '100', '1', '199', '198');
INSERT INTO `rs_admin_attachment` VALUES ('88', '1', '85.png', 'bdc', 'uploads/images/20180803/015ab230174b839f2f83d47fdb60e77c.png', '', '', 'image/png', 'png', '41828', 'b245bc58e31d39948e85da3941213b85', '33164f4cca2e7c91cf173e074868a864111946bf', 'local', '0', '1533279236', '1533279236', '100', '1', '201', '198');
INSERT INTO `rs_admin_attachment` VALUES ('89', '1', '86.png', 'bdc', 'uploads/images/20180803/9c00e3b2cd701b0e3bd136a1e38e78c8.png', '', '', 'image/png', 'png', '181472', '064df113d92b4992ba8ac99d3f0b0c4d', 'ec99301471e1840e61ea5b131a0438000d289883', 'local', '0', '1533279326', '1533279326', '100', '1', '349', '348');
INSERT INTO `rs_admin_attachment` VALUES ('90', '1', '87.png', 'bdc', 'uploads/images/20180803/63a72ab1c8e6bf0946cebe2c8779404a.png', '', '', 'image/png', 'png', '73079', '453e9b3fdf9278a649da883dc6903db5', '039348ef83023cb7e4b31cd79e03f27d5e6cb6e1', 'local', '0', '1533279625', '1533279625', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('91', '1', '88.png', 'bdc', 'uploads/images/20180803/94c2ac542490a8273d7118d016a5b5f0.png', '', '', 'image/png', 'png', '77337', 'f00cd9e08a11bccd7f21e9fba1f894e8', 'e98403f74143fa61b0a4d899a26fb2eba8280e88', 'local', '0', '1533279716', '1533279716', '100', '1', '200', '199');
INSERT INTO `rs_admin_attachment` VALUES ('92', '1', '89.png', 'bdc', 'uploads/images/20180803/ae0cd70d24e32d02d95cb2fea02f5c21.png', '', '', 'image/png', 'png', '67840', 'de4498fa6f43357e74e8d66532b84725', '236c86e56a128efa31f8d554b17b96c9486c2e3b', 'local', '0', '1533280039', '1533280039', '100', '1', '202', '201');
INSERT INTO `rs_admin_attachment` VALUES ('93', '1', '90.png', 'bdc', 'uploads/images/20180803/94a66e0d6ba004acca7a7c81feb7b732.png', '', '', 'image/png', 'png', '36663', '42cae21eeaab8b890377db9f8eed58d5', 'e65028f71eaf5691590f6d567fe183cc23419bdb', 'local', '0', '1533280124', '1533280124', '100', '1', '198', '198');
INSERT INTO `rs_admin_attachment` VALUES ('94', '1', '91.png', 'bdc', 'uploads/images/20180803/c4812abbc28fc3a2002bdd34f3079bac.png', '', '', 'image/png', 'png', '48193', '8fe9789db08a579483cac02090771f91', '0f7b9c26afbfead1a3cc8a15bdcbfefe8b009c06', 'local', '0', '1533280221', '1533280221', '100', '1', '280', '280');
INSERT INTO `rs_admin_attachment` VALUES ('95', '1', '92.png', 'bdc', 'uploads/images/20180803/2adcb2175ca6cf0eee5fe17e781c5d5f.png', '', '', 'image/png', 'png', '69742', '93a3dd77e8ce6a6b6c1a8d7082851804', 'f1b01c8193c3fbfc6452d7b00134a85002eb37d9', 'local', '0', '1533280330', '1533280330', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('96', '1', '93.png', 'bdc', 'uploads/images/20180803/9be74eeac7e2dcee50418454db315ba6.png', '', '', 'image/png', 'png', '72132', 'd99d84a961ce9c3031f0928ab1ae8c1e', '6667622f2acb3ca4d8e1d5fe6dbca83dd1631b4c', 'local', '0', '1533280508', '1533280508', '100', '1', '200', '199');
INSERT INTO `rs_admin_attachment` VALUES ('97', '1', '94.png', 'bdc', 'uploads/images/20180803/f00be4188e45d28f337a03390aa0c9ff.png', '', '', 'image/png', 'png', '103184', '1291087d8ac58d601bac81cd08c5ef8e', '4ec151c84de52fc471b14bb1d4a945eb50d3d1f5', 'local', '0', '1533280593', '1533280593', '100', '1', '199', '200');
INSERT INTO `rs_admin_attachment` VALUES ('98', '1', '95.png', 'bdc', 'uploads/images/20180803/748990a294d375fc85f31c6f5ce15d6f.png', '', '', 'image/png', 'png', '44863', 'b0103a479d6cafbd4cdec9cbbbe9ef06', '1e5ebdbd26a86df2bede40745eda0c535b6cebe9', 'local', '0', '1533280686', '1533280686', '100', '1', '283', '288');
INSERT INTO `rs_admin_attachment` VALUES ('99', '1', '96.png', 'bdc', 'uploads/images/20180803/a1840e354e622b37b944796267807326.png', '', '', 'image/png', 'png', '71759', 'eb82fb6f3ecd6d50cc05b27eabab175e', '3cf84625dd2c3dbd83847399a3138dd6cbe6f76c', 'local', '0', '1533280796', '1533280796', '100', '1', '201', '200');
INSERT INTO `rs_admin_attachment` VALUES ('100', '1', '97.png', 'bdc', 'uploads/images/20180803/3718d0cd2fa0bf1560895380c9f4893e.png', '', '', 'image/png', 'png', '67446', 'b3f2bb9f7c71f05931755caac675c913', 'fab372ca35717d2f14b28f5aaa6c36035b4098c8', 'local', '0', '1533280971', '1533280971', '100', '1', '199', '201');
INSERT INTO `rs_admin_attachment` VALUES ('101', '1', '98.png', 'bdc', 'uploads/images/20180803/d5fb5ac750c3e5e5fac6fd0611de4d57.png', '', '', 'image/png', 'png', '45551', '4a0dc546514f5bd0df1fdda65604a933', '87d63557f3ace3a448a1583f92877886d1b1a901', 'local', '0', '1533281115', '1533281115', '100', '1', '201', '197');
INSERT INTO `rs_admin_attachment` VALUES ('102', '1', '99.png', 'bdc', 'uploads/images/20180803/1c7f10fc2457bb834da031bf204c4119.png', '', '', 'image/png', 'png', '66579', 'ba0a5e5cbda0df919f7611ddac3729cd', '914f080fa3749194e9fd2449d4d97b46263a9171', 'local', '0', '1533281216', '1533281216', '100', '1', '202', '200');
INSERT INTO `rs_admin_attachment` VALUES ('103', '1', '100.png', 'bdc', 'uploads/images/20180803/4119f92a9c766bcb818c43b639767929.png', '', '', 'image/png', 'png', '72795', '7c3b1fc68a010cc4301640f745856cd0', '7b25337c8aaa6e7a9a91578e5974a22a9f738cd5', 'local', '0', '1533281345', '1533281345', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('104', '1', '101.png', 'bdc', 'uploads/images/20180803/0e41df8b05456edfb1786a58e1888915.png', '', '', 'image/png', 'png', '38545', '147dfc0aecf0e910670b65d683a66146', 'a28a350bfc9451cfe409ed8dbc354059191ed6ea', 'local', '0', '1533281510', '1533281510', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('105', '1', '102.png', 'bdc', 'uploads/images/20180803/105a482181ebe927b18a5465c5fa5cc1.png', '', '', 'image/png', 'png', '121000', 'e359a37a7e4c1742009202f2e6580262', '639fc422ed60856682ac4fe73cf70b1ef1537e63', 'local', '0', '1533281627', '1533281627', '100', '1', '349', '348');
INSERT INTO `rs_admin_attachment` VALUES ('106', '1', '103.png', 'bdc', 'uploads/images/20180803/4e5754ee84c9b6a457357de9829593e6.png', '', '', 'image/png', 'png', '36361', 'da294f5d394050e67d1b0dd17973ca32', '81457e85d40edb58b805ae8015208a6def7ccd47', 'local', '0', '1533281750', '1533281750', '100', '1', '203', '203');
INSERT INTO `rs_admin_attachment` VALUES ('107', '1', '104.png', 'bdc', 'uploads/images/20180803/92ef3e8f17b09885efc1069e7c775a71.png', '', '', 'image/png', 'png', '42437', '1447262ea314abf67c59758ad3cd42db', '206beca13d3032f99fcf2ba3855c1bc43c16cbdd', 'local', '0', '1533281834', '1533281834', '100', '1', '244', '224');
INSERT INTO `rs_admin_attachment` VALUES ('108', '1', '105.png', 'bdc', 'uploads/images/20180803/f2e1b9d298a9c1ff25cccc2e372dff71.png', '', '', 'image/png', 'png', '78742', 'd1b3c9689bc4586758ec2ccdc7cf3684', '323859209ff696d36104445b388435ddc601c8ea', 'local', '0', '1533281988', '1533281988', '100', '1', '200', '198');
INSERT INTO `rs_admin_attachment` VALUES ('109', '1', '106.png', 'bdc', 'uploads/images/20180803/5e0f002705557d5496661686d515a4d9.png', '', '', 'image/png', 'png', '91973', '9d41fd982760450571a161b7e1943c84', 'ae430dbd75268a460ca14a78afe119665dd66881', 'local', '0', '1533282099', '1533282099', '100', '1', '202', '202');
INSERT INTO `rs_admin_attachment` VALUES ('110', '1', '107.png', 'bdc', 'uploads/images/20180803/37d9d93ad8e874afc9bd877fbabb700d.png', '', '', 'image/png', 'png', '132711', '5d7b2b92be83e1c6521e6c0066422e2e', 'f70f08ae1c20eaffa944930787bcf94590e8f33a', 'local', '0', '1533282264', '1533282264', '100', '1', '306', '271');
INSERT INTO `rs_admin_attachment` VALUES ('111', '1', '108.png', 'bdc', 'uploads/images/20180803/17498969fa161ec086bb6715fc30f262.png', '', '', 'image/png', 'png', '154172', '3336aec238e83e88466c16adc5ca75fc', '353b519f6a77fb5f9f45c864100e153c27d48d53', 'local', '0', '1533282348', '1533282348', '100', '1', '301', '299');
INSERT INTO `rs_admin_attachment` VALUES ('112', '1', '109.png', 'bdc', 'uploads/images/20180803/47213e5a22cca4e0a44e290aad3d0b3f.png', '', '', 'image/png', 'png', '29392', '5c51933b287754fa291eb14b0c721930', 'acccef72e055a8a25a817efc3b71c59095f7fcd4', 'local', '0', '1533282574', '1533282574', '100', '1', '201', '202');
INSERT INTO `rs_admin_attachment` VALUES ('113', '1', '110.png', 'bdc', 'uploads/images/20180803/a8ec1f8a2709e67ef484dadc1751aeb5.png', '', '', 'image/png', 'png', '68553', '4417ed6a371f61d421a433ea1198c94f', '962229ccacddf5f60a35c3a54d720be1ba122f3a', 'local', '0', '1533282691', '1533282691', '100', '1', '201', '203');
INSERT INTO `rs_admin_attachment` VALUES ('114', '1', '111.png', 'bdc', 'uploads/images/20180803/b007f894a0b851b73211849b949a06fd.png', '', '', 'image/png', 'png', '76662', '980a9bfffcf20f20328ffd3e27210254', 'ac6086f5e0d79360280db5f498f169e36fccc16f', 'local', '0', '1533282849', '1533282849', '100', '1', '201', '199');
INSERT INTO `rs_admin_attachment` VALUES ('115', '1', '112.png', 'bdc', 'uploads/images/20180803/80c8a4ff9fa5bf64661cf77cd261c803.png', '', '', 'image/png', 'png', '200132', 'ed31b3b3ddbc35d24f01458c4750d2a4', '851e46970eca9f5a99e9593099194ee5c1c3c434', 'local', '0', '1533283887', '1533283887', '100', '1', '353', '349');
INSERT INTO `rs_admin_attachment` VALUES ('116', '1', '113.png', 'bdc', 'uploads/images/20180803/0df009d678b5bd8e71f7fe67212900f5.png', '', '', 'image/png', 'png', '109132', '0d9069a9a1c8fcc16d4c838d4c1d4338', '1c58eea8de670e570322d9d377825eba77aefbdf', 'local', '0', '1533284754', '1533284754', '100', '1', '354', '339');
INSERT INTO `rs_admin_attachment` VALUES ('117', '1', '114.png', 'bdc', 'uploads/images/20180803/7e3d174b20abc5974d9607efdd8cdb74.png', '', '', 'image/png', 'png', '303184', '62e8afa98fe1b663bb8dd8f151184d8b', '72094c12caa59287ef02e24593d3aff12388b1b4', 'local', '0', '1533284844', '1533284844', '100', '1', '395', '341');
INSERT INTO `rs_admin_attachment` VALUES ('118', '1', '115.png', 'bdc', 'uploads/images/20180803/930f982e5ef90ec56e46249a0595b3f7.png', '', '', 'image/png', 'png', '32904', 'dc11afed6c7fe75ad4f5ce91ed217401', 'eabdeab98f7683b808aa764b98519b4db9811718', 'local', '0', '1533285010', '1533285010', '100', '1', '198', '199');
INSERT INTO `rs_admin_attachment` VALUES ('119', '1', '116.png', 'bdc', 'uploads/images/20180803/c490e103f6088b6faeb1ffe29e9eb231.png', '', '', 'image/png', 'png', '86780', '826ca092781b6cb2d7af70abc600d986', '97d3c57c2bc3edf22aa8115846613f6df9f311ee', 'local', '0', '1533285117', '1533285117', '100', '1', '202', '202');
INSERT INTO `rs_admin_attachment` VALUES ('120', '1', '117.png', 'bdc', 'uploads/images/20180803/05e015c7badb20297233cc3c72355a7e.png', '', '', 'image/png', 'png', '52060', '11280ff345e877d46b8bd807338dfc08', '379817d2bf91bf4c396364aaa64b88f7e64be9ad', 'local', '0', '1533285213', '1533285213', '100', '1', '203', '203');
INSERT INTO `rs_admin_attachment` VALUES ('121', '1', '118.png', 'bdc', 'uploads/images/20180803/7120a50a41bbd99293717171e64d3db4.png', '', '', 'image/png', 'png', '182289', 'b4c950af2e05b317c0bbb4076b3c05fe', 'c29fafe511bb0b336b511c0e8b676456e7af8223', 'local', '0', '1533285320', '1533285320', '100', '1', '390', '345');
INSERT INTO `rs_admin_attachment` VALUES ('122', '1', '119.png', 'bdc', 'uploads/images/20180803/9a460e878b197ea83400328624e97fbf.png', '', '', 'image/png', 'png', '146286', 'd7883ed000b45a38227c2558985ac959', '5ffb34fd1aafb2d42aac9bd549f759d395966e9a', 'local', '0', '1533285427', '1533285427', '100', '1', '349', '348');
INSERT INTO `rs_admin_attachment` VALUES ('123', '1', '120.png', 'bdc', 'uploads/images/20180803/9ca2d7e684216ac84e49a8d28f9ddf8d.png', '', '', 'image/png', 'png', '73252', 'e328a10ea18cefaa2e93ad5c6bd5d4ed', '84622543451dceb96ecfe136b7ef6149b3ea4f69', 'local', '0', '1533285517', '1533285517', '100', '1', '202', '202');
INSERT INTO `rs_admin_attachment` VALUES ('124', '1', '123.png', 'bdc', 'uploads/images/20180803/fd5b54a5d966cdc1b37847894938f5c7.png', '', '', 'image/png', 'png', '11262', '0988a035b6decd156599b2952fed1f24', '752965e89b771b2b1f2bec6507216436c6984e0a', 'local', '0', '1533286071', '1533286071', '100', '1', '242', '240');
INSERT INTO `rs_admin_attachment` VALUES ('125', '1', '121.png', 'bdc', 'uploads/images/20180803/3d98f889fed6e8d2789776361d0ed10c.png', '', '', 'image/png', 'png', '42296', '0709ab3445e86584817f681e7770d5f6', 'e85c0fb2eeb476da77511d8c801b88919c91212e', 'local', '0', '1533286248', '1533286248', '100', '1', '201', '204');

-- ----------------------------
-- Table structure for `rs_admin_config`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_config`;
CREATE TABLE `rs_admin_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT '' COMMENT '名称',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '标题',
  `group` varchar(32) NOT NULL DEFAULT '' COMMENT '配置分组',
  `type` varchar(32) NOT NULL DEFAULT '' COMMENT '类型',
  `value` text NOT NULL COMMENT '配置值',
  `options` text NOT NULL COMMENT '配置项',
  `tips` varchar(256) NOT NULL DEFAULT '' COMMENT '配置提示',
  `ajax_url` varchar(256) NOT NULL DEFAULT '' COMMENT '联动下拉框ajax地址',
  `next_items` varchar(256) NOT NULL DEFAULT '' COMMENT '联动下拉框的下级下拉框名，多个以逗号隔开',
  `param` varchar(32) NOT NULL DEFAULT '' COMMENT '联动下拉框请求参数名',
  `format` varchar(32) NOT NULL DEFAULT '' COMMENT '格式，用于格式文本',
  `table` varchar(32) NOT NULL DEFAULT '' COMMENT '表名，只用于快速联动类型',
  `level` tinyint(2) unsigned NOT NULL DEFAULT '2' COMMENT '联动级别，只用于快速联动类型',
  `key` varchar(32) NOT NULL DEFAULT '' COMMENT '键字段，只用于快速联动类型',
  `option` varchar(32) NOT NULL DEFAULT '' COMMENT '值字段，只用于快速联动类型',
  `pid` varchar(32) NOT NULL DEFAULT '' COMMENT '父级id字段，只用于快速联动类型',
  `ak` varchar(32) NOT NULL DEFAULT '' COMMENT '百度地图appkey',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态：0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='系统配置表';

-- ----------------------------
-- Records of rs_admin_config
-- ----------------------------
INSERT INTO `rs_admin_config` VALUES ('1', 'web_site_status', '站点开关', 'base', 'switch', '1', '', '站点关闭后将不能访问，后台可正常登录', '', '', '', '', '', '2', '', '', '', '', '1475240395', '1477403914', '1', '1');
INSERT INTO `rs_admin_config` VALUES ('2', 'web_site_title', '站点标题', 'base', 'text', 'BDC社群之家', '', '调用方式：<code>config(\'web_site_title\')</code>', '', '', '', '', '', '2', '', '', '', '', '1475240646', '1477710341', '2', '1');
INSERT INTO `rs_admin_config` VALUES ('3', 'web_site_slogan', '站点标语', 'base', 'text', 'BDC社群之家后台管理系统', '', '站点口号，调用方式：<code>config(\'web_site_slogan\')</code>', '', '', '', '', '', '2', '', '', '', '', '1475240994', '1477710357', '3', '1');
INSERT INTO `rs_admin_config` VALUES ('4', 'web_site_logo', '站点LOGO', 'base', 'image', '', '', '', '', '', '', '', '', '2', '', '', '', '', '1475241067', '1475241067', '4', '1');
INSERT INTO `rs_admin_config` VALUES ('5', 'web_site_description', '站点描述', 'base', 'textarea', '', '', '网站描述，有利于搜索引擎抓取相关信息', '', '', '', '', '', '2', '', '', '', '', '1475241186', '1475241186', '6', '1');
INSERT INTO `rs_admin_config` VALUES ('6', 'web_site_keywords', '站点关键词', 'base', 'text', 'BDC社群之家后台管理系统', '', '网站搜索引擎关键字', '', '', '', '', '', '2', '', '', '', '', '1475241328', '1475241328', '7', '1');
INSERT INTO `rs_admin_config` VALUES ('7', 'web_site_copyright', '版权信息', 'base', 'text', 'Copyright © 2015-2017 BDC社群之家 All rights reserved.', '', '调用方式：<code>config(\'web_site_copyright\')</code>', '', '', '', '', '', '2', '', '', '', '', '1475241416', '1477710383', '8', '1');
INSERT INTO `rs_admin_config` VALUES ('8', 'web_site_icp', '备案信息', 'base', 'text', '', '', '调用方式：<code>config(\'web_site_icp\')</code>', '', '', '', '', '', '2', '', '', '', '', '1475241441', '1477710441', '9', '1');
INSERT INTO `rs_admin_config` VALUES ('9', 'web_site_statistics', '站点统计', 'base', 'textarea', '', '', '网站统计代码，支持百度、Google、cnzz等，调用方式：<code>config(\'web_site_statistics\')</code>', '', '', '', '', '', '2', '', '', '', '', '1475241498', '1477710455', '10', '1');
INSERT INTO `rs_admin_config` VALUES ('10', 'config_group', '配置分组', 'system', 'array', 'base:基本\r\nsystem:系统\r\nupload:上传\r\ndevelop:开发\r\ndatabase:数据库', '', '', '', '', '', '', '', '2', '', '', '', '', '1475241716', '1477649446', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('11', 'form_item_type', '配置类型', 'system', 'array', 'text:单行文本\r\ntextarea:多行文本\r\nstatic:静态文本\r\npassword:密码\r\ncheckbox:复选框\r\nradio:单选按钮\r\ndate:日期\r\ndatetime:日期+时间\r\nhidden:隐藏\r\nswitch:开关\r\narray:数组\r\nselect:下拉框\r\nlinkage:普通联动下拉框\r\nlinkages:快速联动下拉框\r\nimage:单张图片\r\nimages:多张图片\r\nfile:单个文件\r\nfiles:多个文件\r\nueditor:UEditor 编辑器\r\nwangeditor:wangEditor 编辑器\r\neditormd:markdown 编辑器\r\nckeditor:ckeditor 编辑器\r\nicon:字体图标\r\ntags:标签\r\nnumber:数字\r\nbmap:百度地图\r\ncolorpicker:取色器\r\njcrop:图片裁剪\r\nmasked:格式文本\r\nrange:范围\r\ntime:时间', '', '', '', '', '', '', '', '2', '', '', '', '', '1475241835', '1495853193', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('12', 'upload_file_size', '文件上传大小限制', 'upload', 'text', '0', '', '0为不限制大小，单位：kb', '', '', '', '', '', '2', '', '', '', '', '1475241897', '1477663520', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('13', 'upload_file_ext', '允许上传的文件后缀', 'upload', 'tags', 'doc,docx,xls,xlsx,ppt,pptx,pdf,wps,txt,rar,zip,gz,bz2,7z', '', '多个后缀用逗号隔开，不填写则不限制类型', '', '', '', '', '', '2', '', '', '', '', '1475241975', '1477649489', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('14', 'upload_image_size', '图片上传大小限制', 'upload', 'text', '0', '', '0为不限制大小，单位：kb', '', '', '', '', '', '2', '', '', '', '', '1475242015', '1477663529', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('15', 'upload_image_ext', '允许上传的图片后缀', 'upload', 'tags', 'gif,jpg,jpeg,bmp,png', '', '多个后缀用逗号隔开，不填写则不限制类型', '', '', '', '', '', '2', '', '', '', '', '1475242056', '1477649506', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('16', 'list_rows', '分页数量', 'system', 'number', '20', '', '每页的记录数', '', '', '', '', '', '2', '', '', '', '', '1475242066', '1476074507', '101', '1');
INSERT INTO `rs_admin_config` VALUES ('17', 'system_color', '后台配色方案', 'system', 'radio', 'default', 'default:Default\r\namethyst:Amethyst\r\ncity:City\r\nflat:Flat\r\nmodern:Modern\r\nsmooth:Smooth', '', '', '', '', '', '', '2', '', '', '', '', '1475250066', '1477316689', '102', '1');
INSERT INTO `rs_admin_config` VALUES ('18', 'develop_mode', '开发模式', 'develop', 'radio', '0', '0:关闭\r\n1:开启', '', '', '', '', '', '', '2', '', '', '', '', '1476864205', '1476864231', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('19', 'app_trace', '显示页面Trace', 'develop', 'radio', '0', '0:否\r\n1:是', '', '', '', '', '', '', '2', '', '', '', '', '1476866355', '1476866355', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('21', 'data_backup_path', '数据库备份根路径', 'database', 'text', '../data/', '', '路径必须以 / 结尾', '', '', '', '', '', '2', '', '', '', '', '1477017745', '1477018467', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('22', 'data_backup_part_size', '数据库备份卷大小', 'database', 'text', '20971520', '', '该值用于限制压缩后的分卷最大长度。单位：B；建议设置20M', '', '', '', '', '', '2', '', '', '', '', '1477017886', '1477017886', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('23', 'data_backup_compress', '数据库备份文件是否启用压缩', 'database', 'radio', '1', '0:否\r\n1:是', '压缩备份文件需要PHP环境支持 <code>gzopen</code>, <code>gzwrite</code>函数', '', '', '', '', '', '2', '', '', '', '', '1477017978', '1477018172', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('24', 'data_backup_compress_level', '数据库备份文件压缩级别', 'database', 'radio', '9', '1:最低\r\n4:一般\r\n9:最高', '数据库备份文件的压缩级别，该配置在开启压缩时生效', '', '', '', '', '', '2', '', '', '', '', '1477018083', '1477018083', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('25', 'top_menu_max', '顶部导航模块数量', 'system', 'text', '10', '', '设置顶部导航默认显示的模块数量', '', '', '', '', '', '2', '', '', '', '', '1477579289', '1477579289', '103', '1');
INSERT INTO `rs_admin_config` VALUES ('26', 'web_site_logo_text', '站点LOGO文字', 'base', 'image', '', '', '', '', '', '', '', '', '2', '', '', '', '', '1477620643', '1477620643', '5', '1');
INSERT INTO `rs_admin_config` VALUES ('27', 'upload_image_thumb', '缩略图尺寸', 'upload', 'text', '', '', '不填写则不生成缩略图，如需生成 <code>300x300</code> 的缩略图，则填写 <code>300,300</code> ，请注意，逗号必须是英文逗号', '', '', '', '', '', '2', '', '', '', '', '1477644150', '1477649513', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('28', 'upload_image_thumb_type', '缩略图裁剪类型', 'upload', 'radio', '1', '1:等比例缩放\r\n2:缩放后填充\r\n3:居中裁剪\r\n4:左上角裁剪\r\n5:右下角裁剪\r\n6:固定尺寸缩放', '该项配置只有在启用生成缩略图时才生效', '', '', '', '', '', '2', '', '', '', '', '1477646271', '1477649521', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('29', 'upload_thumb_water', '添加水印', 'upload', 'switch', '0', '', '', '', '', '', '', '', '2', '', '', '', '', '1477649648', '1477649648', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('30', 'upload_thumb_water_pic', '水印图片', 'upload', 'image', '', '', '只有开启水印功能才生效', '', '', '', '', '', '2', '', '', '', '', '1477656390', '1477656390', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('31', 'upload_thumb_water_position', '水印位置', 'upload', 'radio', '9', '1:左上角\r\n2:上居中\r\n3:右上角\r\n4:左居中\r\n5:居中\r\n6:右居中\r\n7:左下角\r\n8:下居中\r\n9:右下角', '只有开启水印功能才生效', '', '', '', '', '', '2', '', '', '', '', '1477656528', '1477656528', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('32', 'upload_thumb_water_alpha', '水印透明度', 'upload', 'text', '50', '', '请输入0~100之间的数字，数字越小，透明度越高', '', '', '', '', '', '2', '', '', '', '', '1477656714', '1477661309', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('33', 'wipe_cache_type', '清除缓存类型', 'system', 'checkbox', 'TEMP_PATH', 'TEMP_PATH:应用缓存\r\nLOG_PATH:应用日志\r\nCACHE_PATH:项目模板缓存', '清除缓存时，要删除的缓存类型', '', '', '', '', '', '2', '', '', '', '', '1477727305', '1477727305', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('34', 'captcha_signin', '后台验证码开关', 'system', 'switch', '0', '', '后台登录时是否需要验证码', '', '', '', '', '', '2', '', '', '', '', '1478771958', '1478771958', '99', '1');
INSERT INTO `rs_admin_config` VALUES ('35', 'home_default_module', '前台默认模块', 'system', 'select', 'index', '', '前台默认访问的模块，该模块必须有Index控制器和index方法', '', '', '', '', '', '0', '', '', '', '', '1486714723', '1486715620', '104', '1');
INSERT INTO `rs_admin_config` VALUES ('36', 'minify_status', '开启minify', 'system', 'switch', '0', '', '开启minify会压缩合并js、css文件，可以减少资源请求次数，如果不支持minify，可关闭', '', '', '', '', '', '0', '', '', '', '', '1487035843', '1487035843', '99', '1');
INSERT INTO `rs_admin_config` VALUES ('37', 'upload_driver', '上传驱动', 'upload', 'radio', 'local', 'local:本地', '图片或文件上传驱动', '', '', '', '', '', '0', '', '', '', '', '1501488567', '1501490821', '100', '1');
INSERT INTO `rs_admin_config` VALUES ('38', 'system_log', '系统日志', 'system', 'switch', '1', '', '是否开启系统日志功能', '', '', '', '', '', '0', '', '', '', '', '1512635391', '1512635391', '99', '1');
INSERT INTO `rs_admin_config` VALUES ('39', 'asset_version', '资源版本号', 'develop', 'text', '20180327', '', '可通过修改版号强制用户更新静态文件', '', '', '', '', '', '0', '', '', '', '', '1522143239', '1522143239', '100', '1');

-- ----------------------------
-- Table structure for `rs_admin_hook`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_hook`;
CREATE TABLE `rs_admin_hook` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `plugin` varchar(32) NOT NULL DEFAULT '' COMMENT '钩子来自哪个插件',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子描述',
  `system` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统钩子',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='钩子表';

-- ----------------------------
-- Records of rs_admin_hook
-- ----------------------------
INSERT INTO `rs_admin_hook` VALUES ('1', 'admin_index', '', '后台首页', '1', '1468174214', '1477757518', '1');
INSERT INTO `rs_admin_hook` VALUES ('2', 'plugin_index_tab_list', '', '插件扩展tab钩子', '1', '1468174214', '1468174214', '1');
INSERT INTO `rs_admin_hook` VALUES ('3', 'module_index_tab_list', '', '模块扩展tab钩子', '1', '1468174214', '1468174214', '1');
INSERT INTO `rs_admin_hook` VALUES ('4', 'page_tips', '', '每个页面的提示', '1', '1468174214', '1468174214', '1');
INSERT INTO `rs_admin_hook` VALUES ('5', 'signin_footer', '', '登录页面底部钩子', '1', '1479269315', '1479269315', '1');
INSERT INTO `rs_admin_hook` VALUES ('6', 'signin_captcha', '', '登录页面验证码钩子', '1', '1479269315', '1479269315', '1');
INSERT INTO `rs_admin_hook` VALUES ('7', 'signin', '', '登录控制器钩子', '1', '1479386875', '1479386875', '1');
INSERT INTO `rs_admin_hook` VALUES ('8', 'upload_attachment', '', '附件上传钩子', '1', '1501493808', '1501493808', '1');
INSERT INTO `rs_admin_hook` VALUES ('9', 'page_plugin_js', '', '页面插件js钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('10', 'page_plugin_css', '', '页面插件css钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('11', 'signin_sso', '', '单点登录钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('12', 'signout_sso', '', '单点退出钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('13', 'user_add', '', '添加用户钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('14', 'user_edit', '', '编辑用户钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('15', 'user_delete', '', '删除用户钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('16', 'user_enable', '', '启用用户钩子', '1', '1503633591', '1503633591', '1');
INSERT INTO `rs_admin_hook` VALUES ('17', 'user_disable', '', '禁用用户钩子', '1', '1503633591', '1503633591', '1');

-- ----------------------------
-- Table structure for `rs_admin_hook_plugin`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_hook_plugin`;
CREATE TABLE `rs_admin_hook_plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hook` varchar(32) NOT NULL DEFAULT '' COMMENT '钩子id',
  `plugin` varchar(32) NOT NULL DEFAULT '' COMMENT '插件标识',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='钩子-插件对应表';

-- ----------------------------
-- Records of rs_admin_hook_plugin
-- ----------------------------
INSERT INTO `rs_admin_hook_plugin` VALUES ('3', 'admin_index', 'SystemInfo', '1532789497', '1532789497', '100', '1');

-- ----------------------------
-- Table structure for `rs_admin_icon`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_icon`;
CREATE TABLE `rs_admin_icon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '图标名称',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '图标css地址',
  `prefix` varchar(32) NOT NULL DEFAULT '' COMMENT '图标前缀',
  `font_family` varchar(32) NOT NULL DEFAULT '' COMMENT '字体名',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='图标表';

-- ----------------------------
-- Records of rs_admin_icon
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_admin_icon_list`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_icon_list`;
CREATE TABLE `rs_admin_icon_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `icon_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '所属图标id',
  `title` varchar(128) NOT NULL DEFAULT '' COMMENT '图标标题',
  `class` varchar(255) NOT NULL DEFAULT '' COMMENT '图标类名',
  `code` varchar(128) NOT NULL DEFAULT '' COMMENT '图标关键词',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='详细图标列表';

-- ----------------------------
-- Records of rs_admin_icon_list
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_log`;
CREATE TABLE `rs_admin_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `action_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '行为id',
  `user_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行用户id',
  `action_ip` bigint(20) NOT NULL COMMENT '执行行为者ip',
  `model` varchar(50) NOT NULL DEFAULT '' COMMENT '触发行为的表',
  `record_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '触发行为的数据id',
  `remark` longtext NOT NULL COMMENT '日志备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '执行行为的时间',
  PRIMARY KEY (`id`),
  KEY `action_ip_ix` (`action_ip`),
  KEY `action_id_ix` (`action_id`),
  KEY `user_id_ix` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED COMMENT='行为日志表';

-- ----------------------------
-- Records of rs_admin_log
-- ----------------------------
INSERT INTO `rs_admin_log` VALUES ('1', '2', '1', '2045217323', 'admin_user', '1', '超级管理员 编辑了用户：超级管理员', '1', '1532789301');
INSERT INTO `rs_admin_log` VALUES ('2', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(base)', '1', '1532789573');
INSERT INTO `rs_admin_log` VALUES ('3', '31', '1', '2045217323', 'admin_menu', '1', '超级管理员 编辑了节点：节点ID(1)', '1', '1532791479');
INSERT INTO `rs_admin_log` VALUES ('4', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532791503');
INSERT INTO `rs_admin_log` VALUES ('5', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532791538');
INSERT INTO `rs_admin_log` VALUES ('6', '35', '1', '2045217323', 'admin_module', '0', '超级管理员 安装了模块：门户', '1', '1532791697');
INSERT INTO `rs_admin_log` VALUES ('7', '36', '1', '2045217323', 'admin_module', '0', '超级管理员 卸载了模块：门户', '1', '1532792589');
INSERT INTO `rs_admin_log` VALUES ('8', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(base)', '1', '1532792627');
INSERT INTO `rs_admin_log` VALUES ('9', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(base)', '1', '1532792640');
INSERT INTO `rs_admin_log` VALUES ('10', '35', '1', '2045217323', 'admin_module', '0', '超级管理员 安装了模块：门户', '1', '1532793405');
INSERT INTO `rs_admin_log` VALUES ('11', '35', '1', '2045217323', 'admin_module', '0', '超级管理员 安装了模块：社群管理', '1', '1532797781');
INSERT INTO `rs_admin_log` VALUES ('12', '31', '1', '2045217323', 'admin_menu', '427', '超级管理员 编辑了节点：节点ID(427)', '1', '1532797845');
INSERT INTO `rs_admin_log` VALUES ('13', '31', '1', '2045217323', 'admin_menu', '432', '超级管理员 编辑了节点：节点ID(432)', '1', '1532797855');
INSERT INTO `rs_admin_log` VALUES ('14', '30', '1', '2045217323', 'admin_menu', '433', '超级管理员 添加了节点：所属模块(bdc),所属节点ID(426),节点标题(新增),节点链接(bdc/qun/insert)', '1', '1532798116');
INSERT INTO `rs_admin_log` VALUES ('15', '32', '1', '2045217323', 'admin_menu', '433', '超级管理员 删除了节点：节点ID(433),节点标题(新增),节点链接(bdc/qun/insert)', '1', '1532798144');
INSERT INTO `rs_admin_log` VALUES ('16', '36', '1', '2045217323', 'admin_module', '0', '超级管理员 卸载了模块：门户', '1', '1532798537');
INSERT INTO `rs_admin_log` VALUES ('17', '34', '1', '2045217323', 'admin_menu', '425', '超级管理员 禁用了节点：节点ID(425),节点标题(社群管理),节点链接(bdc/index/index)', '1', '1532798592');
INSERT INTO `rs_admin_log` VALUES ('18', '31', '1', '2045217323', 'admin_menu', '425', '超级管理员 编辑了节点：节点ID(425)', '1', '1532798601');
INSERT INTO `rs_admin_log` VALUES ('19', '33', '1', '2045217323', 'admin_menu', '425', '超级管理员 启用了节点：节点ID(425),节点标题(社群管理),节点链接(bdc/qun/index)', '1', '1532798618');
INSERT INTO `rs_admin_log` VALUES ('20', '30', '1', '2045217323', 'admin_menu', '434', '超级管理员 添加了节点：所属模块(bdc),所属节点ID(424),节点标题(编辑),节点链接(bdc/setting/edit)', '1', '1532798649');
INSERT INTO `rs_admin_log` VALUES ('21', '32', '1', '2045217323', 'admin_menu', '434', '超级管理员 删除了节点：节点ID(434),节点标题(编辑),节点链接(bdc/setting/edit)', '1', '1532799698');
INSERT INTO `rs_admin_log` VALUES ('22', '31', '1', '2045217323', 'admin_menu', '424', '超级管理员 编辑了节点：节点ID(424)', '1', '1532799777');
INSERT INTO `rs_admin_log` VALUES ('23', '31', '1', '2045217323', 'admin_menu', '68', '超级管理员 编辑了节点：节点ID(68)', '1', '1532801366');
INSERT INTO `rs_admin_log` VALUES ('24', '31', '1', '2045217323', 'admin_menu', '4', '超级管理员 编辑了节点：节点ID(4)', '1', '1532801388');
INSERT INTO `rs_admin_log` VALUES ('25', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532801401');
INSERT INTO `rs_admin_log` VALUES ('26', '42', '1', '2045217323', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532801498');
INSERT INTO `rs_admin_log` VALUES ('27', '42', '1', '987247068', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532851713');
INSERT INTO `rs_admin_log` VALUES ('28', '42', '1', '2045217828', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532873057');
INSERT INTO `rs_admin_log` VALUES ('29', '35', '1', '2045217828', 'admin_module', '0', '超级管理员 安装了模块：门户', '1', '1532873198');
INSERT INTO `rs_admin_log` VALUES ('30', '36', '1', '2045217828', 'admin_module', '0', '超级管理员 卸载了模块：门户', '1', '1532873243');
INSERT INTO `rs_admin_log` VALUES ('31', '35', '1', '2045217828', 'admin_module', '0', '超级管理员 安装了模块：门户', '1', '1532873269');
INSERT INTO `rs_admin_log` VALUES ('32', '42', '1', '987247068', 'admin_config', '0', '超级管理员 更新了系统设置：分组(cms)', '1', '1532999143');
INSERT INTO `rs_admin_log` VALUES ('33', '42', '1', '987247068', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532999208');
INSERT INTO `rs_admin_log` VALUES ('34', '42', '1', '987247068', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532999292');
INSERT INTO `rs_admin_log` VALUES ('35', '36', '1', '987247068', 'admin_module', '0', '超级管理员 卸载了模块：门户', '1', '1532999338');
INSERT INTO `rs_admin_log` VALUES ('36', '42', '1', '987247068', 'admin_config', '0', '超级管理员 更新了系统设置：分组(develop)', '1', '1532999356');

-- ----------------------------
-- Table structure for `rs_admin_menu`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_menu`;
CREATE TABLE `rs_admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单id',
  `module` varchar(16) NOT NULL DEFAULT '' COMMENT '模块名称',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '菜单标题',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `url_type` varchar(16) NOT NULL DEFAULT '' COMMENT '链接类型（link：外链，module：模块）',
  `url_value` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `url_target` varchar(16) NOT NULL DEFAULT '_self' COMMENT '链接打开方式：_blank,_self',
  `online_hide` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '网站上线后是否隐藏',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `system_menu` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统菜单，系统菜单不可删除',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `params` varchar(255) NOT NULL DEFAULT '' COMMENT '参数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=807 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of rs_admin_menu
-- ----------------------------
INSERT INTO `rs_admin_menu` VALUES ('1', '0', 'admin', '首页', 'fa fa-fw fa-home', 'module_admin', 'admin/index/index', '_self', '1', '1467617722', '1532791479', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('2', '1', 'admin', '快捷操作', 'fa fa-fw fa-folder-open-o', 'module_admin', '', '_self', '0', '1467618170', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('3', '2', 'admin', '清空缓存', 'fa fa-fw fa-trash-o', 'module_admin', 'admin/index/wipecache', '_self', '0', '1467618273', '1489049773', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('4', '0', 'admin', '系统', 'fa fa-fw fa-gear', 'module_admin', 'admin/system/index', '_self', '1', '1467618361', '1532801388', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('5', '4', 'admin', '系统功能', 'si si-wrench', 'module_admin', '', '_self', '0', '1467618441', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('6', '5', 'admin', '系统设置', 'fa fa-fw fa-wrench', 'module_admin', 'admin/system/index', '_self', '0', '1467618490', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('7', '5', 'admin', '配置管理', 'fa fa-fw fa-gears', 'module_admin', 'admin/config/index', '_self', '0', '1467618618', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('8', '7', 'admin', '新增', '', 'module_admin', 'admin/config/add', '_self', '0', '1467618648', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('9', '7', 'admin', '编辑', '', 'module_admin', 'admin/config/edit', '_self', '0', '1467619566', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('10', '7', 'admin', '删除', '', 'module_admin', 'admin/config/delete', '_self', '0', '1467619583', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('11', '7', 'admin', '启用', '', 'module_admin', 'admin/config/enable', '_self', '0', '1467619609', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('12', '7', 'admin', '禁用', '', 'module_admin', 'admin/config/disable', '_self', '0', '1467619637', '1477710695', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('13', '5', 'admin', '节点管理', 'fa fa-fw fa-bars', 'module_admin', 'admin/menu/index', '_self', '0', '1467619882', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('14', '13', 'admin', '新增', '', 'module_admin', 'admin/menu/add', '_self', '0', '1467619902', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('15', '13', 'admin', '编辑', '', 'module_admin', 'admin/menu/edit', '_self', '0', '1467620331', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('16', '13', 'admin', '删除', '', 'module_admin', 'admin/menu/delete', '_self', '0', '1467620363', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('17', '13', 'admin', '启用', '', 'module_admin', 'admin/menu/enable', '_self', '0', '1467620386', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('18', '13', 'admin', '禁用', '', 'module_admin', 'admin/menu/disable', '_self', '0', '1467620404', '1477710695', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('19', '68', 'user', '权限管理', 'fa fa-fw fa-key', 'module_admin', '', '_self', '0', '1467688065', '1477710702', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('20', '19', 'user', '用户管理', 'fa fa-fw fa-user', 'module_admin', 'user/index/index', '_self', '0', '1467688137', '1477710702', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('21', '20', 'user', '新增', '', 'module_admin', 'user/index/add', '_self', '0', '1467688177', '1477710702', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('22', '20', 'user', '编辑', '', 'module_admin', 'user/index/edit', '_self', '0', '1467688202', '1477710702', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('23', '20', 'user', '删除', '', 'module_admin', 'user/index/delete', '_self', '0', '1467688219', '1477710702', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('24', '20', 'user', '启用', '', 'module_admin', 'user/index/enable', '_self', '0', '1467688238', '1477710702', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('25', '20', 'user', '禁用', '', 'module_admin', 'user/index/disable', '_self', '0', '1467688256', '1477710702', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('211', '64', 'admin', '日志详情', '', 'module_admin', 'admin/log/details', '_self', '0', '1480299320', '1480299320', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('32', '4', 'admin', '扩展中心', 'si si-social-dropbox', 'module_admin', '', '_self', '0', '1467688853', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('33', '32', 'admin', '模块管理', 'fa fa-fw fa-th-large', 'module_admin', 'admin/module/index', '_self', '0', '1467689008', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('34', '33', 'admin', '导入', '', 'module_admin', 'admin/module/import', '_self', '0', '1467689153', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('35', '33', 'admin', '导出', '', 'module_admin', 'admin/module/export', '_self', '0', '1467689173', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('36', '33', 'admin', '安装', '', 'module_admin', 'admin/module/install', '_self', '0', '1467689192', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('37', '33', 'admin', '卸载', '', 'module_admin', 'admin/module/uninstall', '_self', '0', '1467689241', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('38', '33', 'admin', '启用', '', 'module_admin', 'admin/module/enable', '_self', '0', '1467689294', '1477710695', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('39', '33', 'admin', '禁用', '', 'module_admin', 'admin/module/disable', '_self', '0', '1467689312', '1477710695', '6', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('40', '33', 'admin', '更新', '', 'module_admin', 'admin/module/update', '_self', '0', '1467689341', '1477710695', '7', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('41', '32', 'admin', '插件管理', 'fa fa-fw fa-puzzle-piece', 'module_admin', 'admin/plugin/index', '_self', '0', '1467689527', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('42', '41', 'admin', '导入', '', 'module_admin', 'admin/plugin/import', '_self', '0', '1467689650', '1477710695', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('43', '41', 'admin', '导出', '', 'module_admin', 'admin/plugin/export', '_self', '0', '1467689665', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('44', '41', 'admin', '安装', '', 'module_admin', 'admin/plugin/install', '_self', '0', '1467689680', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('45', '41', 'admin', '卸载', '', 'module_admin', 'admin/plugin/uninstall', '_self', '0', '1467689700', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('46', '41', 'admin', '启用', '', 'module_admin', 'admin/plugin/enable', '_self', '0', '1467689730', '1477710695', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('47', '41', 'admin', '禁用', '', 'module_admin', 'admin/plugin/disable', '_self', '0', '1467689747', '1477710695', '6', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('48', '41', 'admin', '设置', '', 'module_admin', 'admin/plugin/config', '_self', '0', '1467689789', '1477710695', '7', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('49', '41', 'admin', '管理', '', 'module_admin', 'admin/plugin/manage', '_self', '0', '1467689846', '1477710695', '8', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('50', '5', 'admin', '附件管理', 'fa fa-fw fa-cloud-upload', 'module_admin', 'admin/attachment/index', '_self', '0', '1467690161', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('51', '70', 'admin', '文件上传', '', 'module_admin', 'admin/attachment/upload', '_self', '0', '1467690240', '1489049773', '1', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('52', '50', 'admin', '下载', '', 'module_admin', 'admin/attachment/download', '_self', '0', '1467690334', '1477710695', '2', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('53', '50', 'admin', '启用', '', 'module_admin', 'admin/attachment/enable', '_self', '0', '1467690352', '1477710695', '3', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('54', '50', 'admin', '禁用', '', 'module_admin', 'admin/attachment/disable', '_self', '0', '1467690369', '1477710695', '4', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('55', '50', 'admin', '删除', '', 'module_admin', 'admin/attachment/delete', '_self', '0', '1467690396', '1477710695', '5', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('56', '41', 'admin', '删除', '', 'module_admin', 'admin/plugin/delete', '_self', '0', '1467858065', '1477710695', '11', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('57', '41', 'admin', '编辑', '', 'module_admin', 'admin/plugin/edit', '_self', '0', '1467858092', '1477710695', '10', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('60', '41', 'admin', '新增', '', 'module_admin', 'admin/plugin/add', '_self', '0', '1467858421', '1477710695', '9', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('61', '41', 'admin', '执行', '', 'module_admin', 'admin/plugin/execute', '_self', '0', '1467879016', '1477710695', '14', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('62', '13', 'admin', '保存', '', 'module_admin', 'admin/menu/save', '_self', '0', '1468073039', '1477710695', '6', '1', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('64', '5', 'admin', '系统日志', 'fa fa-fw fa-book', 'module_admin', 'admin/log/index', '_self', '0', '1476111944', '1477710695', '6', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('65', '5', 'admin', '数据库管理', 'fa fa-fw fa-database', 'module_admin', 'admin/database/index', '_self', '0', '1476111992', '1477710695', '8', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('66', '32', 'admin', '数据包管理', 'fa fa-fw fa-database', 'module_admin', 'admin/packet/index', '_self', '0', '1476112326', '1477710695', '4', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('67', '19', 'user', '角色管理', 'fa fa-fw fa-users', 'module_admin', 'user/role/index', '_self', '0', '1476113025', '1477710702', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('68', '0', 'user', '用户', 'fa fa-fw fa-user', 'module_admin', 'user/index/index', '_self', '1', '1476193348', '1532801366', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('69', '32', 'admin', '钩子管理', 'fa fa-fw fa-anchor', 'module_admin', 'admin/hook/index', '_self', '0', '1476236193', '1477710695', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('70', '2', 'admin', '后台首页', 'fa fa-fw fa-tachometer', 'module_admin', 'admin/index/index', '_self', '0', '1476237472', '1489049773', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('71', '67', 'user', '新增', '', 'module_admin', 'user/role/add', '_self', '0', '1476256935', '1477710702', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('72', '67', 'user', '编辑', '', 'module_admin', 'user/role/edit', '_self', '0', '1476256968', '1477710702', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('73', '67', 'user', '删除', '', 'module_admin', 'user/role/delete', '_self', '0', '1476256993', '1477710702', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('74', '67', 'user', '启用', '', 'module_admin', 'user/role/enable', '_self', '0', '1476257023', '1477710702', '4', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('75', '67', 'user', '禁用', '', 'module_admin', 'user/role/disable', '_self', '0', '1476257046', '1477710702', '5', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('76', '20', 'user', '授权', '', 'module_admin', 'user/index/access', '_self', '0', '1476375187', '1477710702', '6', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('77', '69', 'admin', '新增', '', 'module_admin', 'admin/hook/add', '_self', '0', '1476668971', '1477710695', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('78', '69', 'admin', '编辑', '', 'module_admin', 'admin/hook/edit', '_self', '0', '1476669006', '1477710695', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('79', '69', 'admin', '删除', '', 'module_admin', 'admin/hook/delete', '_self', '0', '1476669375', '1477710695', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('80', '69', 'admin', '启用', '', 'module_admin', 'admin/hook/enable', '_self', '0', '1476669427', '1477710695', '4', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('81', '69', 'admin', '禁用', '', 'module_admin', 'admin/hook/disable', '_self', '0', '1476669564', '1477710695', '5', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('183', '66', 'admin', '安装', '', 'module_admin', 'admin/packet/install', '_self', '0', '1476851362', '1477710695', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('184', '66', 'admin', '卸载', '', 'module_admin', 'admin/packet/uninstall', '_self', '0', '1476851382', '1477710695', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('185', '5', 'admin', '行为管理', 'fa fa-fw fa-bug', 'module_admin', 'admin/action/index', '_self', '0', '1476882441', '1477710695', '7', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('186', '185', 'admin', '新增', '', 'module_admin', 'admin/action/add', '_self', '0', '1476884439', '1477710695', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('187', '185', 'admin', '编辑', '', 'module_admin', 'admin/action/edit', '_self', '0', '1476884464', '1477710695', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('188', '185', 'admin', '启用', '', 'module_admin', 'admin/action/enable', '_self', '0', '1476884493', '1477710695', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('189', '185', 'admin', '禁用', '', 'module_admin', 'admin/action/disable', '_self', '0', '1476884534', '1477710695', '4', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('190', '185', 'admin', '删除', '', 'module_admin', 'admin/action/delete', '_self', '0', '1476884551', '1477710695', '5', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('191', '65', 'admin', '备份数据库', '', 'module_admin', 'admin/database/export', '_self', '0', '1476972746', '1477710695', '1', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('192', '65', 'admin', '还原数据库', '', 'module_admin', 'admin/database/import', '_self', '0', '1476972772', '1477710695', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('193', '65', 'admin', '优化表', '', 'module_admin', 'admin/database/optimize', '_self', '0', '1476972800', '1477710695', '3', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('194', '65', 'admin', '修复表', '', 'module_admin', 'admin/database/repair', '_self', '0', '1476972825', '1477710695', '4', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('195', '65', 'admin', '删除备份', '', 'module_admin', 'admin/database/delete', '_self', '0', '1476973457', '1477710695', '5', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('210', '41', 'admin', '快速编辑', '', 'module_admin', 'admin/plugin/quickedit', '_self', '0', '1477713981', '1477713981', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('209', '185', 'admin', '快速编辑', '', 'module_admin', 'admin/action/quickedit', '_self', '0', '1477713939', '1477713939', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('208', '7', 'admin', '快速编辑', '', 'module_admin', 'admin/config/quickedit', '_self', '0', '1477713808', '1477713808', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('207', '69', 'admin', '快速编辑', '', 'module_admin', 'admin/hook/quickedit', '_self', '0', '1477713770', '1477713770', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('212', '2', 'admin', '个人设置', 'fa fa-fw fa-user', 'module_admin', 'admin/index/profile', '_self', '0', '1489049767', '1489049773', '2', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('213', '70', 'admin', '检查版本更新', '', 'module_admin', 'admin/index/checkupdate', '_self', '0', '1490588610', '1490588610', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('214', '68', 'user', '消息管理', 'fa fa-fw fa-comments-o', 'module_admin', '', '_self', '0', '1520492129', '1520492129', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('215', '214', 'user', '消息列表', 'fa fa-fw fa-th-list', 'module_admin', 'user/message/index', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('216', '215', 'user', '新增', '', 'module_admin', 'user/message/add', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('217', '215', 'user', '编辑', '', 'module_admin', 'user/message/edit', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('218', '215', 'user', '删除', '', 'module_admin', 'user/message/delete', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('219', '215', 'user', '启用', '', 'module_admin', 'user/message/enable', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('220', '215', 'user', '禁用', '', 'module_admin', 'user/message/disable', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('221', '215', 'user', '快速编辑', '', 'module_admin', 'user/message/quickedit', '_self', '0', '1520492195', '1520492195', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('222', '2', 'admin', '消息中心', 'fa fa-fw fa-comments-o', 'module_admin', 'admin/message/index', '_self', '0', '1520495992', '1520496254', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('223', '222', 'admin', '删除', '', 'module_admin', 'admin/message/delete', '_self', '0', '1520495992', '1520496263', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('224', '222', 'admin', '启用', '', 'module_admin', 'admin/message/enable', '_self', '0', '1520495992', '1520496270', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('225', '32', 'admin', '图标管理', 'fa fa-fw fa-tint', 'module_admin', 'admin/icon/index', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('226', '225', 'admin', '新增', '', 'module_admin', 'admin/icon/add', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('227', '225', 'admin', '编辑', '', 'module_admin', 'admin/icon/edit', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('228', '225', 'admin', '删除', '', 'module_admin', 'admin/icon/delete', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('229', '225', 'admin', '启用', '', 'module_admin', 'admin/icon/enable', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('230', '225', 'admin', '禁用', '', 'module_admin', 'admin/icon/disable', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('231', '225', 'admin', '快速编辑', '', 'module_admin', 'admin/icon/quickedit', '_self', '0', '1520908295', '1520908295', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('232', '225', 'admin', '图标列表', '', 'module_admin', 'admin/icon/items', '_self', '0', '1520923368', '1520923368', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('233', '225', 'admin', '更新图标', '', 'module_admin', 'admin/icon/reload', '_self', '0', '1520931908', '1520931908', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('234', '20', 'user', '快速编辑', '', 'module_admin', 'user/index/quickedit', '_self', '0', '1526028258', '1526028258', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('235', '67', 'user', '快速编辑', '', 'module_admin', 'user/role/quickedit', '_self', '0', '1526028282', '1526028282', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('422', '0', 'bdc', '社群', 'fa fa-fw fa-shopping-cart', 'module_admin', 'bdc/index/index', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('423', '422', 'bdc', '系统管理', 'fa fa-fw fa-users', 'module_admin', '', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('424', '423', 'bdc', '基础设置', 'fa fa-fw fa-list', 'module_admin', 'bdc/setting/index', '_self', '0', '1532797781', '1532799777', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('425', '422', 'bdc', '社群管理', 'fa fa-fw fa-shopping-cart', 'module_admin', 'bdc/qun/index', '_self', '0', '1532797781', '1532798601', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('426', '425', 'bdc', '社群列表', 'fa fa-fw fa-list', 'module_admin', 'bdc/qun/index', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('427', '426', 'bdc', '编辑', '', 'module_admin', 'bdc/qun/edit', '_self', '0', '1532797781', '1532797845', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('428', '426', 'bdc', '删除', '', 'module_admin', 'bdc/qun/delete', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('429', '426', 'bdc', '启用', '', 'module_admin', 'bdc/qun/enable', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('430', '426', 'bdc', '禁用', '', 'module_admin', 'bdc/qun/disable', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('431', '426', 'bdc', '快速编辑', '', 'module_admin', 'bdc/qun/quickedit', '_self', '0', '1532797781', '1532797781', '100', '0', '1', '');
INSERT INTO `rs_admin_menu` VALUES ('432', '425', 'bdc', '社群新增', 'fa fa-fw fa-cart-plus', 'module_admin', 'bdc/qun/add', '_self', '0', '1532797781', '1532797856', '100', '0', '1', '');

-- ----------------------------
-- Table structure for `rs_admin_message`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_message`;
CREATE TABLE `rs_admin_message` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uid_receive` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '接收消息的用户id',
  `uid_send` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '发送消息的用户id',
  `type` varchar(128) NOT NULL DEFAULT '' COMMENT '消息分类',
  `content` text NOT NULL COMMENT '消息内容',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `read_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '阅读时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='消息表';

-- ----------------------------
-- Records of rs_admin_message
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_admin_module`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_module`;
CREATE TABLE `rs_admin_module` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '模块名称（标识）',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '模块标题',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '图标',
  `description` text NOT NULL COMMENT '描述',
  `author` varchar(32) NOT NULL DEFAULT '' COMMENT '作者',
  `author_url` varchar(255) NOT NULL DEFAULT '' COMMENT '作者主页',
  `config` text COMMENT '配置信息',
  `access` text COMMENT '授权配置',
  `version` varchar(16) NOT NULL DEFAULT '' COMMENT '版本号',
  `identifier` varchar(64) NOT NULL DEFAULT '' COMMENT '模块唯一标识符',
  `system_module` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否为系统模块',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='模块表';

-- ----------------------------
-- Records of rs_admin_module
-- ----------------------------
INSERT INTO `rs_admin_module` VALUES ('1', 'admin', '系统', 'fa fa-fw fa-gear', '系统模块，DolphinPHP的核心模块', 'DolphinPHP', 'http://www.dolphinphp.com', '', '', '1.0.0', 'admin.dolphinphp.module', '1', '1468204902', '1468204902', '100', '1');
INSERT INTO `rs_admin_module` VALUES ('2', 'user', '用户', 'fa fa-fw fa-user', '用户模块，DolphinPHP自带模块', 'DolphinPHP', 'http://www.dolphinphp.com', '', '', '1.0.0', 'user.dolphinphp.module', '1', '1468204902', '1468204902', '100', '1');
INSERT INTO `rs_admin_module` VALUES ('5', 'bdc', '社群管理', 'fa fa-fw fa-shopping-cart', '社群模块', 'rocky', 'http://www.rockyshi.com', '', '', '1.0.0', 'bdc.rocky.module', '0', '1532797781', '1532873259', '100', '1');

-- ----------------------------
-- Table structure for `rs_admin_packet`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_packet`;
CREATE TABLE `rs_admin_packet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '数据包名',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '数据包标题',
  `author` varchar(32) NOT NULL DEFAULT '' COMMENT '作者',
  `author_url` varchar(255) NOT NULL DEFAULT '' COMMENT '作者url',
  `version` varchar(16) NOT NULL,
  `tables` text NOT NULL COMMENT '数据表名',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='数据包表';

-- ----------------------------
-- Records of rs_admin_packet
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_admin_plugin`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_plugin`;
CREATE TABLE `rs_admin_plugin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '插件名称',
  `title` varchar(32) NOT NULL DEFAULT '' COMMENT '插件标题',
  `icon` varchar(64) NOT NULL DEFAULT '' COMMENT '图标',
  `description` text NOT NULL COMMENT '插件描述',
  `author` varchar(32) NOT NULL DEFAULT '' COMMENT '作者',
  `author_url` varchar(255) NOT NULL DEFAULT '' COMMENT '作者主页',
  `config` text NOT NULL COMMENT '配置信息',
  `version` varchar(16) NOT NULL DEFAULT '' COMMENT '版本号',
  `identifier` varchar(64) NOT NULL DEFAULT '' COMMENT '插件唯一标识符',
  `admin` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台管理',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of rs_admin_plugin
-- ----------------------------
INSERT INTO `rs_admin_plugin` VALUES ('3', 'SystemInfo', '系统环境信息', 'fa fa-fw fa-info-circle', '在后台首页显示服务器信息', '蔡伟明', 'http://www.caiweiming.com', '{\"display\":1,\"width\":6}', '1.0.0', 'system_info.ming.plugin', '0', '1532789497', '1532789497', '100', '1');

-- ----------------------------
-- Table structure for `rs_admin_role`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_role`;
CREATE TABLE `rs_admin_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '上级角色',
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '角色描述',
  `menu_auth` text NOT NULL COMMENT '菜单权限',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态',
  `access` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否可登录后台',
  `default_module` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '默认访问模块',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of rs_admin_role
-- ----------------------------
INSERT INTO `rs_admin_role` VALUES ('1', '0', '超级管理员', '系统默认创建的角色，拥有最高权限', '', '0', '1476270000', '1468117612', '1', '1', '0');

-- ----------------------------
-- Table structure for `rs_admin_user`
-- ----------------------------
DROP TABLE IF EXISTS `rs_admin_user`;
CREATE TABLE `rs_admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(32) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` varchar(96) NOT NULL DEFAULT '' COMMENT '密码',
  `email` varchar(64) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  `email_bind` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否绑定邮箱地址',
  `mobile` varchar(11) NOT NULL DEFAULT '' COMMENT '手机号码',
  `mobile_bind` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否绑定手机号码',
  `avatar` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '头像',
  `money` decimal(11,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '余额',
  `score` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `role` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '角色ID',
  `group` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '部门id',
  `signup_ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '注册ip',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `last_login_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '最后一次登录时间',
  `last_login_ip` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '登录ip',
  `sort` int(11) NOT NULL DEFAULT '100' COMMENT '排序',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态：0禁用，1启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of rs_admin_user
-- ----------------------------
INSERT INTO `rs_admin_user` VALUES ('1', 'admin', '超级管理员', '$2y$10$SYZiebIov/lbMW0b3wkavuVH3/ruv9X1h4zKFp89Z9m1SS0RQm5su', 'admin@admin.com', '0', '', '0', '0', '0.00', '0', '1', '0', '0', '1476065410', '1533311824', '1533311824', '2045217472', '100', '1');

-- ----------------------------
-- Table structure for `rs_bdc_level`
-- ----------------------------
DROP TABLE IF EXISTS `rs_bdc_level`;
CREATE TABLE `rs_bdc_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rs_bdc_level
-- ----------------------------

-- ----------------------------
-- Table structure for `rs_bdc_qun`
-- ----------------------------
DROP TABLE IF EXISTS `rs_bdc_qun`;
CREATE TABLE `rs_bdc_qun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `bianhao` varchar(50) NOT NULL,
  `chicang` int(11) NOT NULL,
  `weixin` varchar(50) NOT NULL,
  `weixinen` varchar(50) NOT NULL,
  `fensi` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `img` varchar(200) NOT NULL,
  `cert_time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rs_bdc_qun
-- ----------------------------
INSERT INTO `rs_bdc_qun` VALUES ('1', 'BDC社群172群', 'BDC172', '300', '许培祥', 'XuPeiXiang', '99', '1532798355', '1533267459', '1', '1', '3', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('4', 'BDC社群103群', 'BDC103', '300', '郑银宝', 'ZhengYinBao', '97', '1533099379', '1533267557', '1', '1', '4', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('5', 'BDC社群105群', 'BDC105', '298', '邵霞', 'Shao Xia', '95', '1533099556', '1533267579', '1', '1', '5', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('6', 'BDC社群175群', 'BDC175', '285', '罗国炷', 'LuoGuoZhu', '90', '1533099971', '1533267635', '1', '1', '6', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('7', 'BDC社群142群', 'BDC142', '285', '雒文杰', 'LuoWenJie', '90', '1533100061', '1533267655', '1', '1', '7', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('8', 'BDC社群174群', 'BDC174', '280', '苏健', 'Su Jian', '85', '1533112696', '1533267682', '1', '1', '8', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('9', 'BDC社群141群', 'BDC141', '280', '李小兵', 'LiXaioBing', '83', '1533112763', '1533267705', '1', '1', '9', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('10', 'BDC社群104群', 'BDC104', '278', '颜振威', 'YanZhenWei', '82', '1533112988', '1533267728', '1', '1', '10', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('11', 'BDC社群118群', 'BDC118', '275', '黄永盛', 'HuangYongSheng', '91', '1533113262', '1533267753', '1', '1', '11', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('12', 'BDC社群501群', 'BDC501', '274', '刘仪明', 'LiuYiMing', '85', '1533113367', '1533267778', '1', '1', '12', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('13', 'BDC社群108群', 'BDC108', '270', '张晓', 'ZhangXiao', '81', '1533113444', '1533267796', '1', '1', '13', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('14', 'BDC社群106群', 'BDC106', '280', '李秀红', 'LiXiuHong', '80', '1533113514', '1533267816', '1', '1', '14', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('15', 'BDC社群111群', 'BDC111', '270', '黄光祥', 'HuangGuangXiang', '90', '1533113575', '1533267357', '1', '1', '15', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('16', 'BDC社群115群', 'BDC115', '278', '鲍翠莲', 'BaoCuiLian', '90', '1533113663', '1533267849', '1', '1', '16', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('17', 'BDC社群110群', 'BDC110', '265', '苏桂月', 'SuGuiYue', '87', '1533114080', '1533267867', '1', '1', '17', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('18', 'BDC社群139群', 'BDC139', '270', '林连华', 'LinLianHua', '86', '1533114206', '1533267892', '1', '1', '18', '2018-08-01');
INSERT INTO `rs_bdc_qun` VALUES ('19', 'BDC社群100群', 'BDC100', '270', '李建', 'Li Jian', '89', '1533263095', '1533267914', '1', '1', '19', '2018-08-02');
INSERT INTO `rs_bdc_qun` VALUES ('20', 'BDC社群101群', 'BDC101', '269', '杨燕', 'Yang Yan', '88', '1533263255', '1533267931', '1', '1', '20', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('21', 'BDC社群123群', 'BDC123', '266', '卫祥', 'Wei Xiang', '87', '1533263364', '1533267953', '1', '1', '21', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('22', 'BDC社群391群', 'BDC391', '265', '李白楚', 'LiBaiChu', '87', '1533263735', '1533267972', '1', '1', '22', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('23', 'BDC社群285群', 'BDC285', '266', '丁卫民', 'DingWeiMin', '88', '1533264071', '1533267996', '1', '1', '23', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('24', 'BDC社群124群', 'BDC124', '265', '李国峰', 'LiGuoFeng', '82', '1533264232', '1533268030', '1', '1', '24', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('25', 'BDC社群153群', 'BDC153', '265', '清晴子', 'QingQingZi', '83', '1533264676', '1533268054', '1', '1', '25', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('26', 'BDC社群121群', 'BDC121', '255', '蒋晓蝶', 'JiangXiaoDie', '85', '1533264867', '1533268077', '1', '1', '26', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('27', 'BDC社群212群', 'BDC212', '263', '张卫检', 'ZhangWeiJian', '82', '1533265031', '1533268103', '1', '1', '27', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('28', 'BDC社群107群', 'BDC107', '260', '周翔', 'Zhou Xiang', '79', '1533265447', '1533268126', '1', '1', '28', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('29', 'BDC社群180群', 'BDC180', '170', '王仔期', 'WangZiQi', '78', '1533265565', '1533268154', '1', '1', '29', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('30', 'BDC社群081群', 'BDC081', '275', '赵颖锐', 'ZhaoYinRui', '83', '1533265803', '1533268179', '1', '1', '30', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('31', 'BDC社群035群', 'BDC035', '650', '赵宇茜', 'Zhao YuXi', '199', '1533265947', '1533268220', '1', '2', '31', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('32', 'BDC社群075群', 'BDC075', '255', '孙湘润', 'SunXiangRun', '80', '1533266219', '1533268253', '1', '1', '32', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('33', 'BDC社群045群', 'BDC045', '255', '李愿', 'Li Yuan', '78', '1533266377', '1533268280', '1', '1', '33', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('34', 'BDC社群066群', 'BDC066', '255', '周依沄', 'Zhou YiYun', '85', '1533266486', '1533268305', '1', '1', '34', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('35', 'BDC社群098群', 'BDC098', '250', '吴华政', 'Wu HuaZheng', '83', '1533266598', '1533268326', '1', '1', '35', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('36', 'BDC社群234群', 'BDC234', '250', '郑丁畅', 'ZhengDingChang', '80', '1533266757', '1533268349', '1', '1', '36', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('37', 'BDC社群145群', 'BDC145', '250', '朱理東', 'Zhu LiDong', '80', '1533266894', '1533268376', '1', '1', '37', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('38', 'BDC社群025群', 'BDC025', '245', '戚漫诗', 'Qi ManShi', '80', '1533268488', '1533268488', '1', '1', '38', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('39', 'BDC社群109群', 'BDC109', '246', '鲁志远', 'Lu ZhiYuan', '80', '1533268602', '1533268602', '1', '1', '39', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('40', 'BDC社群312群', 'BDC312', '245', '乐雨刚', 'Le YuGang', '88', '1533268713', '1533268713', '1', '1', '40', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('41', 'BDC社群091群', 'BDC091', '240', '和盼盼', 'He PanPan', '75', '1533268848', '1533268848', '1', '1', '41', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('42', 'BDC社群075群', 'BDC075', '238', '杜冰洁', 'Du BingJie', '80', '1533269012', '1533269012', '1', '1', '42', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('43', 'BDC社群056群', 'BDC056', '235', '虞浩程', 'Yu HaoCheng', '76', '1533269152', '1533269152', '1', '1', '43', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('44', 'BDC社群128群', 'BDC128', '235', '丁加一', 'Ding JiaYi', '78', '1533269255', '1533269255', '1', '1', '44', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('45', 'BDC社群047群', 'BDC047', '235', '程思琪', 'Cheng SiQi', '77', '1533269390', '1533269390', '1', '1', '45', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('46', 'BDC社群099群', 'BDC099', '233', '芮渝飞', 'Rui YuFei', '76', '1533269503', '1533269503', '1', '1', '46', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('47', 'BDC社群134群', 'BDC134', '230', '牧心然', 'MU XinRan', '75', '1533269593', '1533269593', '1', '1', '47', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('48', 'BDC社群151群', 'BDC151', '229', '宁子锋', 'Ning ZiFeng', '75', '1533269673', '1533269673', '1', '1', '48', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('49', 'BDC社群038群', 'BDC038', '700', '叶彦金', 'Ye YanJin', '198', '1533270419', '1533270419', '1', '2', '49', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('50', 'BDC社群153群', 'BDC153', '690', '钱煜', 'Qian Yu', '197', '1533270527', '1533270527', '1', '2', '50', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('51', 'BDC社群212群', 'BDC212', '690', '秦渝飞', 'Qin YuFei', '195', '1533270667', '1533270667', '1', '2', '51', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('52', 'BDC社群074群', 'BDC074', '685', '谢怡悦', 'Xie YiYue', '188', '1533270776', '1533270776', '1', '2', '52', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('53', 'BDC社群057群', 'BDC057', '690', '韦春米', 'Wei ChunMi', '188', '1533270895', '1533270895', '1', '2', '53', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('54', 'BDC社群125群', 'BDC125', '660', '于成杰', 'Yu ChengJie', '187', '1533271107', '1533271107', '1', '2', '54', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('55', 'BDC社群211群', 'BDC211', '650', '穆润玉', 'Mu RunYu', '188', '1533271218', '1533271218', '1', '2', '55', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('56', 'BDC社群138群', 'BDC138', '670', '阮泽国', 'Ruan ZeGuo', '185', '1533273000', '1533273000', '1', '2', '56', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('57', 'BDC社群351群', 'BDC351', '675', '盛健', 'Sheng Jian', '185', '1533273073', '1533273073', '1', '2', '57', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('58', 'BDC社群183群', 'BDC183', '655', '万飞迪', 'Wan FeiDi', '188', '1533273161', '1533273161', '1', '2', '58', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('59', 'BDC社群072群', 'BDC072', '640', '宣贺', 'Xuan He', '175', '1533273267', '1533273267', '1', '2', '59', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('60', 'BDC社群053群', 'BDC053', '650', '仇兵', 'Chou Bin', '170', '1533273371', '1533273371', '1', '2', '60', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('61', 'BDC社群093群', 'BDC093', '600', '桑菡雪', 'Sang HanXue', '160', '1533273482', '1533273482', '1', '2', '61', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('62', 'BDC社群033群', 'BDC033', '630', '易国泉', 'Yi GuoQuan ', '180', '1533273826', '1533273826', '1', '2', '63', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('63', 'BDC社群017群', 'BDC017', '640', '庄雨汐', 'Zhuang YuXi', '175', '1533273933', '1533273933', '1', '2', '64', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('64', 'BDC社群016群', 'BDC016', '630', '沙龙泽', 'Sha LongZe', '177', '1533274025', '1533274025', '1', '2', '65', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('65', 'BDC社群183群', 'BDC183', '600', '孙涵宇', 'Sun HanYu', '170', '1533274102', '1533274102', '1', '2', '66', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('66', 'BDC社群144群', 'BDC144', '580', '尤广义', 'You GuangYi', '165', '1533274213', '1533274213', '1', '2', '67', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('67', 'BDC社群009群', 'BDC009', '590', '邹艾彬', 'Zou AiBing', '155', '1533274326', '1533274326', '1', '2', '68', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('68', 'BDC社群027群', 'BDC027', '585', '昌璇', 'Chang Xuan', '166', '1533274422', '1533274422', '1', '2', '69', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('69', 'BDC社群111群', 'BDC111', '570', '时昂', 'Shi Ang', '165', '1533274544', '1533274544', '1', '2', '70', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('70', 'BDC社群251群', 'BDC251', '560', '成是非', 'Cheng ShiFei', '158', '1533274654', '1533274654', '1', '2', '71', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('71', 'BDC社群126群', 'BDC126', '630', '林玉峰', 'Lin YuFeng', '169', '1533276991', '1533276991', '1', '2', '72', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('72', 'BDC社群072群', 'BDC072', '625', '林慧明', 'Lin HuiMing', '180', '1533277106', '1533277106', '1', '2', '73', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('73', 'BDC社群043群', 'BDC043', '590', '许晓飞', 'Xu XiaoFei', '155', '1533277228', '1533277228', '1', '2', '74', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('74', 'BDC社群183群', 'BDC183', '570', '马思琪', 'Ma SiQi', '150', '1533277386', '1533277386', '1', '2', '75', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('75', 'BDC社群140群', 'BDC140', '550', '薛成杰', 'Xue ChengJie', '145', '1533277569', '1533277569', '1', '2', '76', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('76', 'BDC社群089群', 'BDC089', '500', '傅辛博', 'Fu XinBo', '150', '1533277723', '1533277723', '1', '2', '77', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('77', 'BDC社群200群', 'BDC200', '480', '尹永杰', 'Yin YongJie', '150', '1533277824', '1533277824', '1', '2', '78', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('78', 'BDC社群099群', 'BDC099', '1000', '戴得鹏', 'Dai DePeng', '298', '1533278124', '1533278531', '1', '3', '79', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('79', 'BDC社群163群', 'BDC163', '1000', '靳李珂', 'Jin LiKe', '295', '1533278262', '1533278262', '1', '3', '80', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('80', 'BDC社群033群', 'BDC033', '980', '谷程壹', 'Gu ChengYi', '290', '1533278366', '1533278366', '1', '3', '81', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('81', 'BDC社群180群', 'BDC180', '950', '司龄宇', 'Si LingYu', '260', '1533278473', '1533278473', '1', '3', '82', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('82', 'BDC社群128群', 'BDC128', '800', '戈海程', 'Ge HaiCheng', '280', '1533278669', '1533278669', '1', '3', '83', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('83', 'BDC社群302群', 'BDC302', '850', '何子锋', 'He ZiFeng', '280', '1533278785', '1533278785', '1', '3', '84', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('84', 'BDC社群072群', 'BDC072', '830', '柏健', 'Bo Jian', '275', '1533278929', '1533278929', '1', '3', '85', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('85', 'BDC社群146群', 'BDC146', '750', '苗志远', 'Miao ZhiYuan', '268', '1533279031', '1533279031', '1', '3', '86', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('86', 'BDC社群153群', 'BDC153', '750', '皮嘉灿', 'Pi JiaCan', '255', '1533279141', '1533279141', '1', '3', '87', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('87', 'BDC社群153群', 'BDC153', '830', '姚大', 'Yao Da', '270', '1533279238', '1533279238', '1', '3', '88', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('88', 'BDC社群236群', 'BDC236', '790', '席梦礼', 'Xi MengLi', '235', '1533279327', '1533279327', '1', '3', '89', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('89', 'BDC社群222群', 'BDC222', '830', '钟晴', 'Zhong Qing', '255', '1533279628', '1533279628', '1', '3', '90', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('90', 'BDC社群119群', 'BDC119', '810', '邓婕', 'Deng Jie', '229', '1533279718', '1533279718', '1', '3', '91', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('91', 'BDC社群231群', 'BDC231', '750', '邱处机', 'Qiu ChuJi', '263', '1533280041', '1533280041', '1', '3', '92', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('92', 'BDC社群241群', 'BDC241', '830', '宋世杰', 'Song ShiJi', '245', '1533280125', '1533280125', '1', '3', '93', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('93', 'BDCs社群155群', 'BDC155', '760', '徐徐飞', 'Xu XuFei', '278', '1533280225', '1533280225', '1', '3', '94', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('94', 'BDC社群157群', 'BDC157', '720', '毕时剑', 'Bi ShiJian', '230', '1533280331', '1533280331', '1', '3', '95', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('95', 'BDC社群148群', 'BDC148', '760', '杨一剑', 'Yang YiJian', '230', '1533280509', '1533280509', '1', '3', '96', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('96', 'BDC社群187群', 'BDC187', '740', '皮阿诺', 'Pi A Nuo', '240', '1533280594', '1533280594', '1', '3', '97', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('97', 'BDC社群228群', 'BDC288', '850', '庞远', 'Pang Yuan ', '245', '1533280688', '1533280688', '1', '3', '98', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('98', 'BDC社群182群', 'BDC182', '900', '夏永正', 'Xia YongZheng', '235', '1533280797', '1533280797', '1', '3', '99', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('99', 'BDC社群160群', 'BDC160', '850', '舒敏', 'Shu Min', '238', '1533280972', '1533280972', '1', '3', '100', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('100', 'BDC社群284群', 'BDC284', '780', '袁时盟', 'Yuan ShiMeng', '288', '1533281116', '1533281116', '1', '3', '101', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('101', 'BDC社群161群', 'BDC161', '800', '骆贵标', 'Luo GuiBiao', '230', '1533281217', '1533281217', '1', '3', '102', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('102', 'BDC社群381群', 'BDC381', '770', '柳音其', 'Liu YinQi', '220', '1533281346', '1533281346', '1', '3', '103', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('103', 'BDC社群358群', 'BDC358', '2000', '高金玲', 'Gao JinLing', '398', '1533281511', '1533281511', '1', '4', '104', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('104', 'BDC社群323群', 'BDC323', '1900', '包雨汐', 'Bao YuXi', '386', '1533281629', '1533281629', '1', '4', '105', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('105', 'BDC社群288群', 'BDC288', '1950', '祖漫诗', 'Zu ManShi', '385', '1533281755', '1533281755', '1', '4', '106', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('106', 'BDC社群274群', 'BDC274', '1800', '屈昂', 'Qu Ang', '356', '1533281836', '1533281836', '1', '4', '107', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('107', 'BDC社群299群', 'BDC299', '1880', '富东宝', 'Fu DongBao', '369', '1533281989', '1533281989', '1', '4', '108', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('108', 'BDC社群320群', 'BDC320', '1700', '江别鹤', 'Jiang BieHe', '345', '1533282101', '1533282101', '1', '4', '109', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('109', 'BDC社群256群', 'BDC256', '1700', '房竣华', 'Fang JunHua', '311', '1533282265', '1533282265', '1', '4', '110', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('110', 'BDC社群235群', 'BDC235', '1650', '童珺婕', 'Tong JunJie', '340', '1533282354', '1533282354', '1', '4', '111', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('111', 'BDC社群193群', 'BDC193', '1600', '巴宇', 'Ba Yu', '369', '1533282576', '1533282576', '1', '4', '112', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('112', 'BDC社群247群', 'BDC247', '1500', '吉怡悦', 'Ji YiYue', '319', '1533282693', '1533282693', '1', '4', '113', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('113', 'BDC社群233群', 'BDC233', '1400', '裴彦心', 'Pei YanXin', '354', '1533282924', '1533282924', '1', '4', '114', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('114', 'BDC社群361群', 'BDC361', '1300', '储嘉鲜', 'Chu JiaXian', '327', '1533283889', '1533283889', '1', '4', '115', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('115', 'BDC社群344群', 'BDC344', '1500', '石浩程', 'Shi HaoCheng', '344', '1533284755', '1533284755', '1', '4', '116', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('116', 'BDC社群277群', 'BDC277', '1800', '宗渝飞', 'Zong YuFei', '377', '1533284845', '1533284845', '1', '4', '117', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('117', 'BDC社群160群', 'BDC160', '1900', '龙郝运', 'Long HanYun', '333', '1533285011', '1533285011', '1', '4', '118', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('118', 'BDC社群327群', 'BDC327', '1350', '封岳骐', 'Feng YueQi', '331', '1533285118', '1533285118', '1', '4', '119', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('119', 'BDC社群228群', 'BDC228', '1400', '曲恒博', 'Qu HengBo', '317', '1533285216', '1533285216', '1', '4', '120', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('120', 'BDC社群093群', 'BDC093', '1450', '梁永义', 'Liang YongYi', '381', '1533285321', '1533285321', '1', '4', '121', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('121', 'BDC社群316群', 'BDC316', '3000', '黄仁波', 'Huang RenBo', '459', '1533285429', '1533285429', '1', '5', '122', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('122', 'BDC社群391群', 'BDC391', '3000', '霍乐迪', 'Huo LeDi', '432', '1533285520', '1533285520', '1', '5', '123', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('123', 'BDC社群316群', 'BDC316', '3000', '纪函雨', 'Ji HanYu', '445', '1533285675', '1533285675', '1', '5', '8', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('124', 'BDC社群374群', 'BDC374', '3000', '宫玺', 'Gong Xi', '412', '1533285956', '1533285956', '1', '5', '41', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('125', 'BDC社群300群', 'BDC300', '3000', '包文博', 'Bao WenBo', '418', '1533286073', '1533286073', '1', '5', '124', '2018-08-03');
INSERT INTO `rs_bdc_qun` VALUES ('126', 'BDC社群301群', 'BDC301', '3000', '魏强', 'Wei Qiang', '421', '1533286250', '1533286250', '1', '5', '125', '2018-08-03');

-- ----------------------------
-- Table structure for `rs_bdc_setting`
-- ----------------------------
DROP TABLE IF EXISTS `rs_bdc_setting`;
CREATE TABLE `rs_bdc_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `wangzhi` varchar(100) NOT NULL,
  `lianxi` varchar(100) NOT NULL,
  `qrcode` varchar(100) DEFAULT NULL,
  `update_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of rs_bdc_setting
-- ----------------------------
INSERT INTO `rs_bdc_setting` VALUES ('1', 'BDC社区生态圈“共享共建”日薪方案', '<p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">一、</span></span><strong><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">社群建设者要求</span></span></strong></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px\">1.积极热心，理解BDC生态，看好BDC发展，共同打造BDC生态圈。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px\">2.具备宣传推广、社群运营及任何对社区发展有帮助的特长，每天可以为BDC社区贡献4个小时以上的时间建设社群。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\">&nbsp;</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">二、</span></span><strong><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">社群建设者权益与义务</span></span></strong></p><p><span style=\";font-family:&#39;Times New Roman&#39;;display:none;font-size:16px\">&nbsp;</span></p><table><tbody><tr class=\"firstRow\"><td width=\"422\" valign=\"center\" colspan=\"4\" style=\"padding: 0px; border-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><strong><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">BDC社群经济——日薪方案</span></strong></p></td></tr><tr><td width=\"99\" valign=\"center\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">级别</span></p></td><td width=\"104\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top-width: 1px; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">建群人数</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top-width: 1px; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">持仓数量</span><span style=\"font-family: Calibri;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">/BDC</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top-width: 1px; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">日薪奖励</span><span style=\"font-family: Calibri;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">/BDC</span></p></td></tr><tr><td width=\"99\" valign=\"center\" rowspan=\"3\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">一级群</span></p></td><td width=\"104\" valign=\"center\" rowspan=\"3\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">50-99人</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">100</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">2</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">200</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">4</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">300</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">6</span></p></td></tr><tr><td width=\"99\" valign=\"center\" rowspan=\"3\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">二级群</span></p></td><td width=\"104\" valign=\"center\" rowspan=\"3\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">100-199人</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">300</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">8</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">500</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">10</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">700</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">14</span></p></td></tr><tr><td width=\"99\" valign=\"center\" rowspan=\"2\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">三级群</span></p></td><td width=\"104\" valign=\"center\" rowspan=\"2\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">200-299人</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">700</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">18</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">1000</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">25</span></p></td></tr><tr><td width=\"99\" valign=\"center\" rowspan=\"2\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">四级群</span></p></td><td width=\"104\" valign=\"center\" rowspan=\"2\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">300-399人</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">1200</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">30</span></p></td></tr><tr><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">2000</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">40</span></p></td></tr><tr><td width=\"99\" valign=\"center\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">五级群</span></p></td><td width=\"104\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">400-500人</span></p></td><td width=\"112\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">3000</span></p></td><td width=\"107\" valign=\"center\" style=\"padding: 0px; border-left: none; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">60</span></p></td></tr><tr><td width=\"422\" valign=\"center\" colspan=\"4\" style=\"padding: 0px; border-left-width: 1px; border-right-width: 1px; border-top: none; border-bottom-width: 1px;\"><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-align:center;line-height:30px\"><span style=\"font-family: &#39;Microsoft YaHei UI&#39;;color: rgb(255, 255, 255);letter-spacing: 0;font-size: 14px;background: rgb(201, 0, 0)\">以上奖励均每日发放！</span></p></td></tr></tbody></table><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px;background: rgb(254, 254, 254)\">&nbsp;</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px;background: rgb(254, 254, 254)\">1、建立50—99人微信社群的为一级群，同时群主拥有BDC为100/200/300每天可分别获得2/4/6枚BDC的奖励。当BDC10元/枚为基数，一级群每日收益分别约为20/40/60元人民币，每月收益分别约为600/1200/1800元人民币。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px;background: rgb(254, 254, 254)\">2、建立100—199人微信社群的为二级群，同时群主拥有BDC为300/500/700每天分别可获得8/10/14枚BDC的奖励。当BDC10元/枚为基数，二级群每日收益分别约为80/100/140元人民币，每月收益分别约为2400/3000/4200元人民币</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">3、建立200—299人微信社群的为三级群，同时群主拥有BDC为700/1000每天分别可获得18/25枚BDC的奖励。当BDC10元/枚为基数，三级群每日收益分别约为180/250元人民币，每月收益分别约为5400/7500元人民币。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">4、建立100—199人微信社群的为四级群，同时群主拥有BDC为1200/2000每天分别可获得30/40枚BDC的奖励。当BDC10元/枚为基数，四级群每日收益分别约为300/400元人民币，每月收益分别约为9000/12000元人民币。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">5、建立400—500人微信社群的为五级群，群主拥有BDC为3000每天分别可获得60枚BDC的奖励及其他奖励。当BDC10元/枚为基数，五级群每日收益分别约为600元人民币，每月收益分别约为18000元人民币。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">6、凡推荐别人建立微信共享社群者，推荐人一次性可获得20枚BDC的推荐奖励。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">7、BDC将视情况不定期给予社群建设者特别奖励，以激励为社区作出重大贡献的社群建设者</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px;background: rgb(254, 254, 254)\">&nbsp;</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">三、</span></span><strong><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 16px\"><span style=\"font-family:宋体\">社群建设者任务及考核</span></span></strong></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">1、社群建设者可以在BDC团队的指导下，充分发挥自身优势，自行决定如何推广和维护BDC。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 14px\">2.&nbsp;社群建设者定期将自身工作及成果汇报给BDC团队，我们将综合评估志愿者的工作，给予合理的激励。</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px\"><span style=\"font-family:宋体\">愿意参与</span>BDC事业的社区成员，请主动加官方社群微信客服：即可！</span></p><p style=\"margin-top:0;margin-right:0;margin-bottom:0;margin-left:0;text-indent:0;padding:0 0 0 0 ;text-align:justify;text-justify:inter-ideograph;line-height:30px;background:rgb(255,255,255)\"><span style=\"font-family: 宋体;color: rgb(51, 51, 51);letter-spacing: 0;font-size: 15px\"><span style=\"font-family:宋体\">注：政策及方案会随着时间有所变化，最终解释权归迪拜品牌数字货币交易所</span></span></p>', 'www.brandchain.club', 'brandchain@outlook.com', '62', '1533273604');
